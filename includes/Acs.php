<?php
/**
 * @file
 * The Acs.php file for the miniorange_samlauth module.
 *
 * @package MiniOrange
 *
 * @license GNU/GPLv3
 *
 * @copyright Copyright 2015 miniOrange. All Rights Reserved.
 *
 * This file is part of miniOrange SAML plugin.
 */

include 'Response.php';
/**
 * MiniOrangeSamlauthAcs class.
 */
class MiniOrangeSamlauthAcs {

  /**
   * The function processSamlResponse.
   */
  public function processSamlResponse() {
    if (array_key_exists('SAMLResponse', $_POST)) {
      $saml_response = $_POST['SAMLResponse'];
    }
    else {
      throw new Exception('Missing SAMLRequest or SAMLResponse parameter.');
    }

    $saml_response = base64_decode($saml_response);
    $document = new DOMDocument();
    $document->loadXML($saml_response);
    $saml_response_xml = $document->firstChild;

    $saml_response = new MiniOrangeSamlauthResponse($saml_response_xml);
    $acs_url = $GLOBALS['base_url'] . '/miniorange_samlauth/acs';
    $issuer = variable_get('miniorange_samlauth_saml_idp_entityid');
    $cert_fingerprint = variable_get('miniorange_samlauth_saml_idp_cert_fp');

    /* convert to UTF-8 character encoding */
    $cert_fingerprint = iconv("UTF-8", "CP1252//IGNORE", $cert_fingerprint);

    /* remove whitespaces */
    $cert_fingerprint = preg_replace('/\s+/', '', $cert_fingerprint);

    $signature_data = MiniOrangeSamlauthUtilities::validateElement($saml_response_xml);

    if ($signature_data !== FALSE) {
      $valid_signature = MiniOrangeSamlauthUtilities::validateResponse($acs_url, $cert_fingerprint, $signature_data, $saml_response);
      if ($valid_signature === FALSE) {
        throw new Exception('Invalid signature.');
      }
    }

    // Verify the issuer and audience from saml response.
    MiniOrangeSamlauthUtilities::validateIssuerAndAudience($saml_response, $acs_url, $issuer);

    $username = $saml_response->getAssertions()[0]->getNameId()['Value'];

    return $username;
  }

}
