<?php
/**
 * @file
 * The Utilities.php file for the miniorange_samlauth module.
 *
 * This file is part of miniOrange SAML plugin.
 */

include "xmlseclibs.php";
/**
 * The class MiniOrangeSamlauthUtilities.
 */
class MiniOrangeSamlauthUtilities {

  /**
   * The function generateId.
   */
  public static function generateId() {
    return '_' . self::stringToHex(self::generateRandomBytes(21));
  }

  /**
   * The function stringToHex.
   */
  public static function stringToHex($bytes) {
    $ret = '';
    for ($i = 0; $i < strlen($bytes); $i++) {
      $ret .= sprintf('%02x', ord($bytes[$i]));
    }
    return $ret;
  }

  /**
   * The function generateRandomBytes.
   */
  public static function generateRandomBytes($length, $fallback = TRUE) {
    assert('is_int($length)');
    return openssl_random_pseudo_bytes($length);
  }

  /**
   * The function createAuthnRequest.
   */
  public static function createAuthnRequest($acs_url, $issuer) {
    $request_xml_str = '<?xml version="1.0" encoding="UTF-8"?>' . '<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="' . self::generateId() . '" Version="2.0" IssueInstant="' . self::generateTimestamp() . '" ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" AssertionConsumerServiceURL="' . $acs_url . '" ><saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">' . $issuer . '</saml:Issuer></samlp:AuthnRequest>';
    $deflated_str = gzdeflate($request_xml_str);
    $base64_encoded_str = base64_encode($deflated_str);
    $url_encoded = urlencode($base64_encoded_str);
    return $url_encoded;
  }

  /**
   * The function generateTimestamp.
   */
  public static function generateTimestamp($instant = NULL) {
    if ($instant === NULL) {
      $instant = time();
    }
    return gmdate('Y-m-d\TH:i:s\Z', $instant);
  }

  /**
   * The function xpQuery.
   */
  public static function xpQuery(DOMNode $node, $query) {
    assert('is_string($query)');
    static $xp_cache = NULL;

    if ($node instanceof DOMDocument) {
      $doc = $node;
    }
    else {
      $doc = $node->ownerDocument;
    }

    if ($xp_cache === NULL || !$xp_cache->document->isSameNode($doc)) {
      $xp_cache = new DOMXPath($doc);
      // $xp_cache->registerNamespace('soap-env', SAML2_Const::NS_SOAP);
      $xp_cache->registerNamespace('saml_protocol', 'urn:oasis:names:tc:SAML:2.0:protocol');
      $xp_cache->registerNamespace('saml_assertion', 'urn:oasis:names:tc:SAML:2.0:assertion');
      $xp_cache->registerNamespace('saml_metadata', 'urn:oasis:names:tc:SAML:2.0:metadata');
      $xp_cache->registerNamespace('ds', 'http://www.w3.org/2000/09/xmldsig#');
      $xp_cache->registerNamespace('xenc', 'http://www.w3.org/2001/04/xmlenc#');
    }

    $results = $xp_cache->query($query, $node);
    $ret = array();
    for ($i = 0; $i < $results->length; $i++) {
      $ret[$i] = $results->item($i);
    }

    return $ret;
  }

  /**
   * The function parseNameId.
   */
  public static function parseNameId(DOMElement $xml) {
    $ret = array('Value' => trim($xml->textContent));

    foreach (array('NameQualifier', 'SPNameQualifier', 'Format') as $attr) {
      if ($xml->hasAttribute($attr)) {
        $ret[$attr] = $xml->getAttribute($attr);
      }
    }

    return $ret;
  }

  /**
   * The function xsDateTimeToTimestamp.
   */
  public static function xsDateTimeToTimestamp($time) {
    $matches = array();

    // We use a very strict regex to parse the timestamp.
    $regex = '/^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)T(\\d\\d):(\\d\\d):(\\d\\d)(?:\\.\\d+)?Z$/D';
    if (preg_match($regex, $time, $matches) == 0) {
      throw new Exception('Invalid SAML2 timestamp passed to xsDateTimeToTimestamp: ' . $time);
    }

    // Extract the different components of the time from the matches in the regex.
    // intval will ignore leading zeroes in the string.
    $year = intval($matches[1]);
    $month = intval($matches[2]);
    $day = intval($matches[3]);
    $hour = intval($matches[4]);
    $minute = intval($matches[5]);
    $second = intval($matches[6]);

    // We use gmmktime because the timestamp will always be given
    // in UTC.
    $ts = gmmktime($hour, $minute, $second, $month, $day, $year);

    return $ts;
  }

  /**
   * The function extractStrings.
   */
  public static function extractStrings(DOMElement $parent, $namespace_uri, $local_name) {
    assert('is_string($namespace_uri)');
    assert('is_string($local_name)');

    $ret = array();
    for ($node = $parent->firstChild; $node !== NULL; $node = $node->nextSibling) {
      if ($node->namespaceURI !== $namespace_uri || $node->localName !== $local_name) {
        continue;
      }
      $ret[] = trim($node->textContent);
    }

    return $ret;
  }

  /**
   * The function validateElement.
   */
  public static function validateElement(DOMElement $root) {
    /* Create an XML security object. */
    $obj_xml_sec_dsig = new XMLSecurityDSig();

    /* Both SAML messages and SAML assertions use the 'ID' attribute. */
    $obj_xml_sec_dsig->idKeys[] = 'ID';

    /* Locate the XMLDSig Signature element to be used. */
    $signature_element = self::xpQuery($root, './ds:Signature');
    if (count($signature_element) === 0) {
      /* We don't have a signature element to validate. */
      return FALSE;
    }
    elseif (count($signature_element) > 1) {
      throw new Exception('XMLSec: more than one signature element in root.');
    }
    $signature_element = $signature_element[0];
    $obj_xml_sec_dsig->sigNode = $signature_element;

    /* Canonicalize the XMLDSig SignedInfo element in the message. */
    $obj_xml_sec_dsig->canonicalizeSignedInfo();

    /* Validate referenced xml nodes. */
    if (!$obj_xml_sec_dsig->validateReference()) {
      throw new Exception('XMLsec: digest validation failed');
    }

    /* Check that $root is one of the signed nodes. */
    $root_signed = FALSE;

    foreach ($obj_xml_sec_dsig->getValidatedNodes() as $signed_node) {
      if ($signed_node->isSameNode($root)) {
        $root_signed = TRUE;
        break;
      }
      elseif ($root->parentNode instanceof DOMDocument && $signed_node->isSameNode($root->ownerDocument)) {
        /* $root is the root element of a signed document. */
        $root_signed = TRUE;
        break;
      }
    }
    if (!$root_signed) {
      throw new Exception('XMLSec: The root element is not signed.');
    }

    /* Now we extract all available X509 certificates in the signature element. */
    $certificates = array();
    foreach (self::xpQuery($signature_element, './ds:KeyInfo/ds:X509Data/ds:X509Certificate') as $cert_node) {
      $cert_data = trim($cert_node->textContent);
      $cert_data = str_replace(array("\r", "\n", "\t", ' '), '', $cert_data);
      $certificates[] = $cert_data;

    }

    $ret = array('Signature' => $obj_xml_sec_dsig, 'Certificates' => $certificates);

    return $ret;
  }


  /**
   * The function validateSignature.
   */
  public static function validateSignature(array $info, XMLSecurityKey $key) {
    assert('array_key_exists("Signature", $info)');

    $obj_xml_sec_dsig = $info['Signature'];

    $sig_method = self::xpQuery($obj_xml_sec_dsig->sigNode, './ds:SignedInfo/ds:SignatureMethod');
    if (empty($sig_method)) {
      throw new Exception('Missing SignatureMethod element.');
    }
    $sig_method = $sig_method[0];
    if (!$sig_method->hasAttribute('Algorithm')) {
      throw new Exception('Missing Algorithm-attribute on SignatureMethod element.');
    }
    $algo = $sig_method->getAttribute('Algorithm');

    if ($key->type === XMLSecurityKey::RSA_SHA1 && $algo !== $key->type) {
      $key = self::castKey($key, $algo);
    }

    /* Check the signature. */
    if (!$obj_xml_sec_dsig->verify($key)) {
      throw new Exception("Unable to validate Signature");
    }
  }

  /**
   * The function validateResponse.
   */
  public static function validateResponse($current_url, $cert_fingerprint, $signature_data, MiniOrangeSamlauthResponse $response) {
    assert('is_string($current_url)');
    assert('is_string($cert_fingerprint)');

    /* Validate Response-element destination. */
    $msg_destination = $response->getDestination();
    if ($msg_destination !== NULL && $msg_destination !== $current_url) {
      throw new Exception('Destination in response doesn\'t match the current URL. Destination is "' . $msg_destination . '", current URL is "' . $current_url . '".');
    }

    $response_signed = self::checkSign($cert_fingerprint, $signature_data);

    /* Returning boolean $response_signed */
    return $response_signed;
  }

  /**
   * The function checkSign.
   */
  public static function checkSign($cert_fingerprint, $signature_data) {
    $certificates = $signature_data['Certificates'];

    if (count($certificates) === 0) {
      echo 'No certificate in message when validating against fingerprint.';
      return FALSE;
    }
    else {
      echo 'Found ' . count($certificates) . ' certificates.';
    }

    $fp_array = array();
    $fp_array[] = $cert_fingerprint;
    $pem_cert = self::findCertificate($fp_array, $certificates);

    $last_exception = NULL;

    $key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array(
      'type' => 'public',
    ));
    $key->loadKey($pem_cert);

    try {
      /*
       * Make sure that we have a valid signature
       */
      assert('$key->type === XMLSecurityKey::RSA_SHA1');
      self::validateSignature($signature_data, $key);
      echo 'Validation with key succeeded.';
      return TRUE;
    }
    catch (Exception $e) {
      echo 'Validation with key failed with exception: ' . $e->getMessage();
      $last_exception = $e;
    }

    /* We were unable to validate the signature with any of our keys. */
    if ($last_exception !== NULL) {
      throw $last_exception;
    }
    else {
      return FALSE;
    }
  }

  /**
   * The function validateIssuerAndAudience.
   */
  public static function validateIssuerAndAudience(MiniOrangeSamlauthResponse $saml_response, $acs_url, $issuer_to_validate_against) {
    $issuer = $saml_response->getAssertions()[0]->getIssuer();
    $audience = $saml_response->getAssertions()[0]->getValidAudiences()[0];

    if (strcmp($issuer_to_validate_against, $issuer) === 0) {

      if (strcmp($audience, $acs_url) === 0) {
        return TRUE;
      }
      else {
        throw new Exception('Invalid audience.');
      }
    }
    else {
      throw new Exception('Issuer cannot be verified.');
    }
  }

  /**
   * The function findCertificate.
   */
  private static function findCertificate(array $cert_fingerprints, array $certificates) {

    // $candidates = array();
    foreach ($certificates as $cert) {
      $fp = strtolower(sha1(base64_decode($cert)));
      if (!in_array($fp, $cert_fingerprints, TRUE)) {
        // $candidates[] = $fp;
        continue;
      }

      /* We have found a matching fingerprint. */
      $pem = "-----BEGIN CERTIFICATE-----\n" . chunk_split($cert, 64) . "-----END CERTIFICATE-----\n";
      return $pem;
    }

    throw new Exception('Unable to find a certificate matching the configured fingerprint.');
  }

}
