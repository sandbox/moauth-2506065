<?php
/**
 * @file
 * The Assertion.php file for the miniorange_samlauth module.
 *
 * This file is part of miniOrange SAML plugin.
 */

include 'Utilities.php';
/**
 * The MiniOrangeSamlauthAssertion class.
 */
class MiniOrangeSamlauthAssertion {
  private $id;
  private $issueInstant;
  private $issuer;
  private $nameId;
  private $encryptedNameId;
  private $encryptedAttribute;
  private $encryptionKey;
  private $notBefore;
  private $notOnOrAfter;
  private $validAudiences;
  private $sessionNotOnOrAfter;
  private $sessionIndex;
  private $authnInstant;
  private $authnContextClassRef;
  private $authnContextDecl;
  private $authnContextDeclRef;
  private $AuthenticatingAuthority;
  private $attributes;
  private $nameFormat;
  private $signatureKey;
  private $certificates;
  private $signatureData;
  private $requiredEncAttributes;
  private $subjectConfirmation;
  protected $wasSignedAtConstruction = FALSE;

  /**
   * The constructor function.
   */
  public function __construct(DOMElement $xml = NULL) {
    $this->id = MiniOrangeSamlauthUtilities::generateId();
    $this->issueInstant = MiniOrangeSamlauthUtilities::generateTimestamp();
    $this->issuer = '';
    $this->authnInstant = MiniOrangeSamlauthUtilities::generateTimestamp();
    $this->attributes = array();
    $this->nameFormat = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
    $this->certificates = array();
    $this->AuthenticatingAuthority = array();
    $this->subjectConfirmation = array();

    if ($xml === NULL) {
      return;
    }

    if (!$xml->hasAttribute('ID')) {
      throw new Exception('Missing ID attribute on SAML assertion.');
    }
    $this->id = $xml->getAttribute('ID');

    if ($xml->getAttribute('Version') !== '2.0') {
      /* Currently a very strict check. */
      throw new Exception('Unsupported version: ' . $xml->getAttribute('Version'));
    }

    $this->issueInstant = MiniOrangeSamlauthUtilities::xsDateTimeToTimestamp($xml->getAttribute('IssueInstant'));

    $issuer = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:Issuer');
    if (empty($issuer)) {
      throw new Exception('Missing <saml:Issuer> in assertion.');
    }
    $this->issuer = trim($issuer[0]->textContent);

    $this->parseSubject($xml);
    $this->parseConditions($xml);
    $this->parseAuthnStatement($xml);
    $this->parseAttributes($xml);

    $this->parseSignature($xml);

  }

  /**
   * Parse subject in assertion.
   *
   * @param DOMElement $xml
   *          The assertion XML element.
   */
  private function parseSubject(DOMElement $xml) {
    $subject = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:Subject');
    if (empty($subject)) {
      /* No Subject node. */

      return;
    }
    elseif (count($subject) > 1) {
      throw new Exception('More than one <saml:Subject> in <saml:Assertion>.');
    }

    $subject = $subject[0];

    $name_id = MiniOrangeSamlauthUtilities::xpQuery($subject, './saml_assertion:NameID | ./saml_assertion:EncryptedID/xenc:EncryptedData');
    if (empty($name_id)) {
      throw new Exception('Missing <saml:NameID> or <saml:EncryptedID> in <saml:Subject>.');
    }
    elseif (count($name_id) > 1) {
      throw new Exception('More than one <saml:NameID> or <saml:EncryptedD> in <saml:Subject>.');
    }
    $name_id = $name_id[0];
    if ($name_id->localName === 'EncryptedData') {
      /* The NameID element is encrypted. */
      $this->encryptedNameId = $name_id;
    }
    else {
      $this->nameId = MiniOrangeSamlauthUtilities::parseNameId($name_id);
    }

  }

  /**
   * Parse conditions in assertion.
   *
   * @param DOMElement $xml
   *          The assertion XML element.
   */
  private function parseConditions(DOMElement $xml) {
    $conditions = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:Conditions');
    if (empty($conditions)) {
      /* No <saml:Conditions> node. */

      return;
    }
    elseif (count($conditions) > 1) {
      throw new Exception('More than one <saml:Conditions> in <saml:Assertion>.');
    }
    $conditions = $conditions[0];

    if ($conditions->hasAttribute('NotBefore')) {
      $not_before = MiniOrangeSamlauthUtilities::xsDateTimeToTimestamp($conditions->getAttribute('NotBefore'));
      if ($this->notBefore === NULL || $this->notBefore < $not_before) {
        $this->notBefore = $not_before;
      }
    }
    if ($conditions->hasAttribute('NotOnOrAfter')) {
      $not_on_or_after = MiniOrangeSamlauthUtilities::xsDateTimeToTimestamp($conditions->getAttribute('NotOnOrAfter'));
      if ($this->notOnOrAfter === NULL || $this->notOnOrAfter > $not_on_or_after) {
        $this->notOnOrAfter = $not_on_or_after;
      }
    }

    for ($node = $conditions->firstChild; $node !== NULL; $node = $node->nextSibling) {
      if ($node instanceof DOMText) {
        continue;
      }
      if ($node->namespaceURI !== 'urn:oasis:names:tc:SAML:2.0:assertion') {
        throw new Exception('Unknown namespace of condition: ' . var_export($node->namespaceURI, TRUE));
      }
      switch ($node->localName) {
        case 'AudienceRestriction':
          $audiences = MiniOrangeSamlauthUtilities::extractStrings($node, 'urn:oasis:names:tc:SAML:2.0:assertion', 'Audience');
          if ($this->validAudiences === NULL) {
            /* The first (and probably last) AudienceRestriction element. */
            $this->validAudiences = $audiences;
          }
          else {
            /*
             * The set of AudienceRestriction are ANDed together, so we need
             * the subset that are present in all of them.
             */
            $this->validAudiences = array_intersect($this->validAudiences, $audiences);
          }
          break;

        case 'OneTimeUse':
          /* Currently ignored. */
          break;

        case 'ProxyRestriction':
          /* Currently ignored. */
          break;

        default:
          throw new Exception('Unknown condition: ' . var_export($node->localName, TRUE));
      }
    }
  }

  /**
   * Parse AuthnStatement in assertion.
   *
   * @param DOMElement $xml
   *          The assertion XML element.
   */
  private function parseAuthnStatement(DOMElement $xml) {
    $authn_statements = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:AuthnStatement');
    if (empty($authn_statements)) {
      $this->authnInstant = NULL;

      return;
    }
    elseif (count($authn_statements) > 1) {
      throw new Exception('More that one <saml:AuthnStatement> in <saml:Assertion> not supported.');
    }
    $authn_statement = $authn_statements[0];

    if (!$authn_statement->hasAttribute('AuthnInstant')) {
      throw new Exception('Missing required AuthnInstant attribute on <saml:AuthnStatement>.');
    }
    $this->authnInstant = MiniOrangeSamlauthUtilities::xsDateTimeToTimestamp($authn_statement->getAttribute('AuthnInstant'));

    if ($authn_statement->hasAttribute('SessionNotOnOrAfter')) {
      $this->sessionNotOnOrAfter = MiniOrangeSamlauthUtilities::xsDateTimeToTimestamp($authn_statement->getAttribute('SessionNotOnOrAfter'));
    }

    if ($authn_statement->hasAttribute('SessionIndex')) {
      $this->sessionIndex = $authn_statement->getAttribute('SessionIndex');
    }

    $this->parseAuthnContext($authn_statement);
  }

  /**
   * Parse AuthnContext in AuthnStatement.
   */
  private function parseAuthnContext(DOMElement $authn_statement_el) {
    // Get the AuthnContext element.
    $authn_contexts = MiniOrangeSamlauthUtilities::xpQuery($authn_statement_el, './saml_assertion:AuthnContext');
    if (count($authn_contexts) > 1) {
      throw new Exception('More than one <saml:AuthnContext> in <saml:AuthnStatement>.');
    }
    elseif (empty($authn_contexts)) {
      throw new Exception('Missing required <saml:AuthnContext> in <saml:AuthnStatement>.');
    }
    $authn_context_el = $authn_contexts[0];

    // Get the AuthnContextDeclRef (if available).
    $authn_context_decl_refs = MiniOrangeSamlauthUtilities::xpQuery($authn_context_el, './saml_assertion:AuthnContextDeclRef');
    if (count($authn_context_decl_refs) > 1) {
      throw new Exception('More than one <saml:AuthnContextDeclRef> found?');
    }
    elseif (count($authn_context_decl_refs) === 1) {
      $this->setAuthnContextDeclRef(trim($authn_context_decl_refs[0]->textContent));
    }

    // Get the AuthnContextDecl (if available).
    $authn_context_decls = MiniOrangeSamlauthUtilities::xpQuery($authn_context_el, './saml_assertion:AuthnContextDecl');
    if (count($authn_context_decls) > 1) {
      throw new Exception('More than one <saml:AuthnContextDecl> found?');
    }
    elseif (count($authn_context_decls) === 1) {
      $this->setAuthnContextDecl(new SAML2_XML_Chunk($authn_context_decls[0]));
    }

    // Get the AuthnContextClassRef (if available).
    $authn_context_class_refs = MiniOrangeSamlauthUtilities::xpQuery($authn_context_el, './saml_assertion:AuthnContextClassRef');
    if (count($authn_context_class_refs) > 1) {
      throw new Exception('More than one <saml:AuthnContextClassRef> in <saml:AuthnContext>.');
    }
    elseif (count($authn_context_class_refs) === 1) {
      $this->setAuthnContextClassRef(trim($authn_context_class_refs[0]->textContent));
    }

    // Constraint from XSD: MUST have one of the three.
    if (empty($this->authnContextClassRef) && empty($this->authnContextDecl) && empty($this->authnContextDeclRef)) {
      throw new Exception('Missing either <saml:AuthnContextClassRef> or <saml:AuthnContextDeclRef> or <saml:AuthnContextDecl>');
    }

    $this->AuthenticatingAuthority = MiniOrangeSamlauthUtilities::extractStrings($authn_context_el, 'urn:oasis:names:tc:SAML:2.0:assertion', 'AuthenticatingAuthority');
  }

  /**
   * Parse attribute statements in assertion.
   *
   * @param DOMElement $xml
   *          The XML element with the assertion.
   */
  private function parseAttributes(DOMElement $xml) {
    $first_attribute = TRUE;
    $attributes = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:AttributeStatement/saml_assertion:Attribute');
    foreach ($attributes as $attribute) {
      if (!$attribute->hasAttribute('Name')) {
        throw new Exception('Missing name on <saml:Attribute> element.');
      }
      $name = $attribute->getAttribute('Name');

      if ($attribute->hasAttribute('NameFormat')) {
        $name_format = $attribute->getAttribute('NameFormat');
      }
      else {
        $name_format = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
      }

      if ($first_attribute) {
        $this->nameFormat = $name_format;
        $first_attribute = FALSE;
      }
      else {
        if ($this->nameFormat !== $name_format) {
          $this->nameFormat = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
        }
      }

      if (!array_key_exists($name, $this->attributes)) {
        $this->attributes[$name] = array();
      }

      $values = MiniOrangeSamlauthUtilities::xpQuery($attribute, './saml_assertion:AttributeValue');
      foreach ($values as $value) {
        $this->attributes[$name][] = trim($value->textContent);
      }
    }
  }

  /**
   * Parse encrypted attribute statements in assertion.
   *
   * @param DOMElement $xml
   *          The XML element with the assertion.
   */
  private function parseEncryptedAttributes(DOMElement $xml) {
    $this->encryptedAttribute = MiniOrangeSamlauthUtilities::xpQuery($xml, './saml_assertion:AttributeStatement/saml_assertion:EncryptedAttribute');
  }

  /**
   * Parse signature on assertion.
   *
   * @param DOMElement $xml
   *          The assertion XML element.
   */
  private function parseSignature(DOMElement $xml) {
    /* Validate the signature element of the message. */
    $sig = MiniOrangeSamlauthUtilities::validateElement($xml);
    if ($sig !== FALSE) {
      $this->wasSignedAtConstruction = TRUE;
      $this->certificates = $sig['Certificates'];
      $this->signatureData = $sig;
    }
  }

  /**
   * Validate this assertion against a public key.
   *
   * If no signature was present on the assertion, we will return FALSE.
   * Otherwise, TRUE will be returned. An exception is thrown if the
   * signature validation fails.
   *
   * @param XMLSecurityKey $key
   *          The key we should check against.
   */
  public function validate(XMLSecurityKey $key) {
    assert('$key->type === XMLSecurityKey::RSA_SHA1');

    if ($this->signatureData === NULL) {
      return FALSE;
    }

    MiniOrangeSamlauthUtilities::validateSignature($this->signatureData, $key);

    return TRUE;
  }

  /**
   * Retrieve the identifier of this assertion.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the identifier of this assertion.
   *
   * @param string $id
   *          The new identifier of this assertion.
   */
  public function setId($id) {
    assert('is_string($id)');

    $this->id = $id;
  }

  /**
   * Retrieve the issue timestamp of this assertion.
   */
  public function getIssueInstant() {
    return $this->issueInstant;
  }

  /**
   * Set the issue timestamp of this assertion.
   *
   * @param int $issue_instant
   *          The new issue timestamp of this assertion, as an UNIX timestamp.
   */
  public function setIssueInstant($issue_instant) {
    assert('is_int($issue_instant)');

    $this->issueInstant = $issue_instant;
  }

  /**
   * Retrieve the issuer if this assertion.
   */
  public function getIssuer() {
    return $this->issuer;
  }

  /**
   * Set the issuer of this message.
   *
   * @param string $issuer
   *          The new issuer of this assertion.
   */
  public function setIssuer($issuer) {
    assert('is_string($issuer)');

    $this->issuer = $issuer;
  }

  /**
   * Retrieve the NameId of the subject in the assertion.
   *
   * The returned NameId is in the format used by MiniOrangeSamlauthUtilities::addNameId().
   *
   * @see MiniOrangeSamlauthUtilities::addNameId()
   */
  public function getNameId() {
    if ($this->encryptedNameId !== NULL) {
      throw new Exception('Attempted to retrieve encrypted NameID without decrypting it first.');
    }

    return $this->nameId;
  }

  /**
   * Set the NameId of the subject in the assertion.
   *
   * The NameId must be in the format accepted by MiniOrangeSamlauthUtilities::addNameId().
   *
   * @see MiniOrangeSamlauthUtilities::addNameId()
   */
  public function setNameId($name_id) {
    assert('is_array($name_id) || is_null($name_id)');

    $this->nameId = $name_id;
  }

  /**
   * Check whether the NameId is encrypted.
   */
  public function isNameIdEncrypted() {
    if ($this->encryptedNameId !== NULL) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Encrypt the NameID in the Assertion.
   *
   * @param XMLSecurityKey $key
   *          The encryption key.
   */
  public function encryptNameId(XMLSecurityKey $key) {
    /* First create a XML representation of the NameID. */
    $doc = new DOMDocument();
    $root = $doc->createElement('root');
    $doc->appendChild($root);
    MiniOrangeSamlauthUtilities::addNameId($root, $this->nameId);
    $name_id = $root->firstChild;

    MiniOrangeSamlauthUtilities::getContainer()->debugMessage($name_id, 'encrypt');

    /* Encrypt the NameID. */
    $enc = new XMLSecEnc();
    $enc->setNode($name_id);

    $enc->type = XMLSecEnc::ELEMENT;

    $symmetric_key = new XMLSecurityKey(XMLSecurityKey::AES128_CBC);
    $symmetric_key->generateSessionKey();
    $enc->encryptKey($key, $symmetric_key);

    $this->encryptedNameId = $enc->encryptNode($symmetric_key);
    $this->nameId = NULL;
  }

  /**
   * Decrypt the NameId of the subject in the assertion.
   *
   * @param XMLSecurityKey $key
   *          The decryption key.
   * @param array $blacklist
   *          Blacklisted decryption algorithms.
   */
  public function decryptNameId(XMLSecurityKey $key, array $blacklist = array()) {
    if ($this->encryptedNameId === NULL) {
      /* No NameID to decrypt. */

      return;
    }

    $name_id = MiniOrangeSamlauthUtilities::decryptElement($this->encryptedNameId, $key, $blacklist);
    MiniOrangeSamlauthUtilities::getContainer()->debugMessage($name_id, 'decrypt');
    $this->nameId = MiniOrangeSamlauthUtilities::parseNameId($name_id);

    $this->encryptedNameId = NULL;
  }

  /**
   * Decrypt the assertion attributes.
   */
  public function decryptAttributes(XMLSecurityKey $key, array $blacklist = array()) {
    if ($this->encryptedAttribute === NULL) {
      return;
    }
    $first_attribute = TRUE;
    $attributes = $this->encryptedAttribute;
    foreach ($attributes as $attribute_enc) {
      /* Decrypt node <EncryptedAttribute> */
      $attribute = MiniOrangeSamlauthUtilities::decryptElement($attribute_enc->getElementsByTagName('EncryptedData')->item(0), $key, $blacklist);

      if (!$attribute->hasAttribute('Name')) {
        throw new Exception('Missing name on <saml:Attribute> element.');
      }
      $name = $attribute->getAttribute('Name');

      if ($attribute->hasAttribute('NameFormat')) {
        $name_format = $attribute->getAttribute('NameFormat');
      }
      else {
        $name_format = 'urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified';
      }

      if ($first_attribute) {
        $this->nameFormat = $name_format;
        $first_attribute = FALSE;
      }
      else {
        if ($this->nameFormat !== $name_format) {
          $this->nameFormat = 'urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified';
        }
      }

      if (!array_key_exists($name, $this->attributes)) {
        $this->attributes[$name] = array();
      }

      $values = MiniOrangeSamlauthUtilities::xpQuery($attribute, './saml_assertion:AttributeValue');
      foreach ($values as $value) {
        $this->attributes[$name][] = trim($value->textContent);
      }
    }
  }

  /**
   * Retrieve the earliest timestamp this assertion is valid.
   *
   * This function returns NULL if there are no restrictions on how early the
   * assertion can be used.
   */
  public function getNotBefore() {
    return $this->notBefore;
  }

  /**
   * Set the earliest timestamp this assertion can be used.
   *
   * Set this to NULL if no limit is required.
   *
   * @param int|NULL $not_before
   *          The earliest timestamp this assertion is valid.
   */
  public function setNotBefore($not_before) {
    assert('is_int($not_before) || is_null($not_before)');

    $this->notBefore = $not_before;
  }

  /**
   * Retrieve the expiration timestamp of this assertion.
   *
   * This function returns NULL if there are no restrictions on how
   * late the assertion can be used.
   */
  public function getNotOnOrAfter() {
    return $this->notOnOrAfter;
  }

  /**
   * Set the expiration timestamp of this assertion.
   *
   * Set this to NULL if no limit is required.
   *
   * @param int|NULL $not_on_or_after
   *          The latest timestamp this assertion is valid.
   */
  public function setNotOnOrAfter($not_on_or_after) {
    assert('is_int($not_on_or_after) || is_null($not_on_or_after)');

    $this->notOnOrAfter = $not_on_or_after;
  }

  /**
   * Set $EncryptedAttributes if attributes will send encrypted.
   *
   * @param bool $ea
   *          TRUE to encrypt attributes in the assertion.
   */
  public function setEncryptedAttributes($ea) {
    $this->requiredEncAttributes = $ea;
  }

  /**
   * Retrieve the audiences that are allowed to receive this assertion.
   *
   * This may be NULL, in which case all audiences are allowed.
   */
  public function getValidAudiences() {
    return $this->validAudiences;
  }

  /**
   * Set the audiences that are allowed to receive this assertion.
   *
   * This may be NULL, in which case all audiences are allowed.
   *
   * @param array|NULL $valid_audiences
   *          The allowed audiences.
   */
  public function setValidAudiences(array $valid_audiences = NULL) {
    $this->validAudiences = $valid_audiences;
  }

  /**
   * Retrieve the AuthnInstant of the assertion.
   */
  public function getAuthnInstant() {
    return $this->authnInstant;
  }

  /**
   * Set the AuthnInstant of the assertion.
   *
   * @param int|NULL $authn_instant
   *          Timestamp the user was authenticated, or NULL if we don't want an AuthnStatement.
   */
  public function setAuthnInstant($authn_instant) {
    assert('is_int($authn_instant) || is_null($authn_instant)');

    $this->authnInstant = $authn_instant;
  }

  /**
   * Retrieve the session expiration timestamp.
   *
   * This function returns NULL if there are no restrictions on the
   * session lifetime.
   */
  public function getSessionNotOnOrAfter() {
    return $this->sessionNotOnOrAfter;
  }

  /**
   * Set the session expiration timestamp.
   *
   * Set this to NULL if no limit is required.
   *
   * @param int|NULL $session_not_on_or_after
   *          The latest timestamp this session is valid.
   */
  public function setSessionNotOnOrAfter($session_not_on_or_after) {
    assert('is_int($session_not_on_or_after) || is_null($session_not_on_or_after)');

    $this->sessionNotOnOrAfter = $session_not_on_or_after;
  }

  /**
   * Retrieve the session index of the user at the IdP.
   */
  public function getSessionIndex() {
    return $this->sessionIndex;
  }

  /**
   * Set the session index of the user at the IdP.
   *
   * Note that the authentication context must be set before the
   * session index can be inluded in the assertion.
   *
   * @param string|NULL $session_index
   *          The session index of the user at the IdP.
   */
  public function setSessionIndex($session_index) {
    assert('is_string($session_index) || is_null($session_index)');

    $this->sessionIndex = $session_index;
  }

  /**
   * Retrieve the authentication method used to authenticate the user.
   *
   * This will return NULL if no authentication statement was
   * included in the assertion.
   *
   * Note that this returns either the AuthnContextClassRef or the AuthnConextDeclRef, whose definition overlaps
   * but is slightly different (consult the specification for more information).
   * This was done to work around an old bug of Shibboleth ( https://bugs.internet2.edu/jira/browse/SIDP-187 ).
   * Should no longer be required, please use either getAuthnConextClassRef or getAuthnContextDeclRef.
   *
   * @deprecated use getAuthnContextClassRef
   */
  public function getAuthnContext() {
    if (!empty($this->authnContextClassRef)) {
      return $this->authnContextClassRef;
    }
    if (!empty($this->authnContextDeclRef)) {
      return $this->authnContextDeclRef;
    }
    return NULL;
  }

  /**
   * Set the authentication method used to authenticate the user.
   *
   * If this is set to NULL, no authentication statement will be
   * included in the assertion. The default is NULL.
   *
   * @deprecated use setAuthnContextClassRef
   */
  public function setAuthnContext($authn_context) {
    $this->setAuthnContextClassRef($authn_context);
  }

  /**
   * Retrieve the authentication method used to authenticate the user.
   *
   * This will return NULL if no authentication statement was
   * included in the assertion.
   */
  public function getAuthnContextClassRef() {
    return $this->authnContextClassRef;
  }

  /**
   * Set the authentication method used to authenticate the user.
   *
   * If this is set to NULL, no authentication statement will be
   * included in the assertion. The default is NULL.
   *
   * @param string|NULL $authn_context_class_ref
   *          The authentication method.
   */
  public function setAuthnContextClassRef($authn_context_class_ref) {
    assert('is_string($authn_context_class_ref) || is_null($authn_context_class_ref)');

    $this->authnContextClassRef = $authn_context_class_ref;
  }

  /**
   * Set the authentication context declaration.
   */
  public function setAuthnContextDecl(SAML2_XML_Chunk $authn_context_decl) {
    if (!empty($this->authnContextDeclRef)) {
      throw new Exception('AuthnContextDeclRef is already registered! May only have either a Decl or a DeclRef, not both!');
    }

    $this->authnContextDecl = $authn_context_decl;
  }

  /**
   * Get the authentication context declaration.
   *
   * See:
   *
   * @url http://docs.oasis-open.org/security/saml/v2.0/saml-authn-context-2.0-os.pdf
   */
  public function getAuthnContextDecl() {
    return $this->authnContextDecl;
  }

  /**
   * Set the authentication context declaration reference.
   */
  public function setAuthnContextDeclRef($authn_context_decl_ref) {
    if (!empty($this->authnContextDecl)) {
      throw new Exception('AuthnContextDecl is already registered! May only have either a Decl or a DeclRef, not both!');
    }

    $this->authnContextDeclRef = $authn_context_decl_ref;
  }

  /**
   * Get the authentication context declaration reference.
   *
   * URI reference that identifies an authentication context declaration.
   *
   * The URI reference MAY directly resolve into an XML document containing the referenced declaration.
   */
  public function getAuthnContextDeclRef() {
    return $this->authnContextDeclRef;
  }

  /**
   * Retrieve the AuthenticatingAuthority.
   */
  public function getAuthenticatingAuthority() {
    return $this->AuthenticatingAuthority;
  }

  /**
   * Set the AuthenticatingAuthority.
   */
  public function setAuthenticatingAuthority($authenticating_authority) {
    $this->AuthenticatingAuthority = $authenticating_authority;
  }

  /**
   * Retrieve all attributes.
   */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
   * Replace all attributes.
   *
   * @param array $attributes
   *          All new attributes, as an associative array.
   */
  public function setAttributes(array $attributes) {
    $this->attributes = $attributes;
  }

  /**
   * Retrieve the NameFormat used on all attributes.
   *
   * If more than one NameFormat is used in the received attributes, this
   * returns the unspecified NameFormat.
   */
  public function getAttributeNameFormat() {
    return $this->nameFormat;
  }

  /**
   * Set the NameFormat used on all attributes.
   *
   * @param string $name_format
   *          The NameFormat used on all attributes.
   */
  public function setAttributeNameFormat($name_format) {
    assert('is_string($name_format)');

    $this->nameFormat = $name_format;
  }

  /**
   * Retrieve the subject_confirmation elements we have in our Subject element.
   */
  public function getSubjectConfirmation() {
    return $this->subjectConfirmation;
  }

  /**
   * Set the subject_confirmation elements that should be included in the assertion.
   *
   * @param array $subject_confirmation
   *          Array of SAML2_XML_saml_SubjectConfirmation elements.
   */
  public function setSubjectConfirmation(array $subject_confirmation) {
    $this->subjectConfirmation = $subject_confirmation;
  }

  /**
   * Retrieve the private key we should use to sign the assertion.
   */
  public function getSignatureKey() {
    return $this->signatureKey;
  }

  /**
   * Set the private key we should use to sign the assertion.
   *
   * If the key is NULL, the assertion will be sent unsigned.
   */
  public function setSignatureKey(XMLsecurityKey $signature_key = NULL) {
    $this->signatureKey = $signature_key;
  }

  /**
   * Return the key we should use to encrypt the assertion.
   */
  public function getEncryptionKey() {
    return $this->encryptionKey;
  }

  /**
   * Set the private key we should use to encrypt the attributes.
   */
  public function setEncryptionKey(XMLSecurityKey $key = NULL) {
    $this->encryptionKey = $key;
  }

  /**
   * Set the certificates that should be included in the assertion.
   *
   * The certificates should be strings with the PEM encoded data.
   *
   * @param array $certificates
   *          An array of certificates.
   */
  public function setCertificates(array $certificates) {
    $this->certificates = $certificates;
  }

  /**
   * Retrieve the certificates that are included in the assertion.
   */
  public function getCertificates() {
    return $this->certificates;
  }

  /**
   * The function getWasSignedAtConstruction.
   */
  public function getWasSignedAtConstruction() {
    return $this->wasSignedAtConstruction;
  }

  /**
   * Convert this assertion to an XML element.
   */
  public function toXml(DOMNode $parent_element = NULL) {
    if ($parent_element === NULL) {
      $document = new DOMDocument();
      $parent_element = $document;
    }
    else {
      $document = $parent_element->ownerDocument;
    }

    $root = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:' . 'Assertion');
    $parent_element->appendChild($root);

    /* Ugly hack to add another namespace declaration to the root element. */
    $root->setAttributeNS('urn:oasis:names:tc:SAML:2.0:protocol', 'samlp:tmp', 'tmp');
    $root->removeAttributeNS('urn:oasis:names:tc:SAML:2.0:protocol', 'tmp');
    $root->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:tmp', 'tmp');
    $root->removeAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'tmp');
    $root->setAttributeNS('http://www.w3.org/2001/XMLSchema', 'xs:tmp', 'tmp');
    $root->removeAttributeNS('http://www.w3.org/2001/XMLSchema', 'tmp');

    $root->setAttribute('ID', $this->id);
    $root->setAttribute('Version', '2.0');
    $root->setAttribute('IssueInstant', gmdate('Y-m-d\TH:i:s\Z', $this->issueInstant));

    $issuer = MiniOrangeSamlauthUtilities::addString($root, 'urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Issuer', $this->issuer);

    $this->addSubject($root);
    $this->addConditions($root);
    $this->addAuthnStatement($root);
    if ($this->requiredEncAttributes == FALSE) {
      $this->addAttributeStatement($root);
    }
    else {
      $this->addEncryptedAttributeStatement($root);
    }

    if ($this->signatureKey !== NULL) {
      MiniOrangeSamlauthUtilities::insertSignature($this->signatureKey, $this->certificates, $root, $issuer->nextSibling);
    }

    return $root;
  }

  /**
   * Add a Subject-node to the assertion.
   *
   * @param DOMElement $root
   *          The assertion element we should add the subject to.
   */
  private function addSubject(DOMElement $root) {
    if ($this->nameId === NULL && $this->encryptedNameId === NULL) {
      /* We don't have anything to create a Subject node for. */

      return;
    }

    $subject = $root->ownerDocument->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Subject');
    $root->appendChild($subject);

    if ($this->encryptedNameId === NULL) {
      MiniOrangeSamlauthUtilities::addNameId($subject, $this->nameId);
    }
    else {
      $eid = $subject->ownerDocument->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:' . 'EncryptedID');
      $subject->appendChild($eid);
      $eid->appendChild($subject->ownerDocument->importNode($this->encryptedNameId, TRUE));
    }

    foreach ($this->subjectConfirmation as $sc) {
      $sc->toXml($subject);
    }
  }

  /**
   * Add a Conditions-node to the assertion.
   *
   * @param DOMElement $root
   *          The assertion element we should add the conditions to.
   */
  private function addConditions(DOMElement $root) {
    $document = $root->ownerDocument;

    $conditions = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Conditions');
    $root->appendChild($conditions);

    if ($this->notBefore !== NULL) {
      $conditions->setAttribute('NotBefore', gmdate('Y-m-d\TH:i:s\Z', $this->notBefore));
    }
    if ($this->notOnOrAfter !== NULL) {
      $conditions->setAttribute('NotOnOrAfter', gmdate('Y-m-d\TH:i:s\Z', $this->notOnOrAfter));
    }

    if ($this->validAudiences !== NULL) {
      $ar = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AudienceRestriction');
      $conditions->appendChild($ar);

      MiniOrangeSamlauthUtilities::addStrings($ar, 'urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Audience', FALSE, $this->validAudiences);
    }
  }

  /**
   * Add a AuthnStatement-node to the assertion.
   *
   * @param DOMElement $root
   *          The assertion element we should add the authentication statement to.
   */
  private function addAuthnStatement(DOMElement $root) {
    if ($this->authnInstant === NULL || ($this->authnContextClassRef === NULL && $this->authnContextDecl === NULL && $this->authnContextDeclRef === NULL)) {
      /* No authentication context or AuthnInstant => no authentication statement. */

      return;
    }

    $document = $root->ownerDocument;

    $authn_statement_el = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AuthnStatement');
    $root->appendChild($authn_statement_el);

    $authn_statement_el->setAttribute('AuthnInstant', gmdate('Y-m-d\TH:i:s\Z', $this->authnInstant));

    if ($this->sessionNotOnOrAfter !== NULL) {
      $authn_statement_el->setAttribute('SessionNotOnOrAfter', gmdate('Y-m-d\TH:i:s\Z', $this->sessionNotOnOrAfter));
    }
    if ($this->sessionIndex !== NULL) {
      $authn_statement_el->setAttribute('SessionIndex', $this->sessionIndex);
    }

    $authn_context_el = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AuthnContext');
    $authn_statement_el->appendChild($authn_context_el);

    if (!empty($this->authnContextClassRef)) {
      MiniOrangeSamlauthUtilities::addString($authn_context_el, 'urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AuthnContextClassRef', $this->authnContextClassRef);
    }
    if (!empty($this->authnContextDecl)) {
      $this->authnContextDecl->toXml($authn_context_el);
    }
    if (!empty($this->authnContextDeclRef)) {
      MiniOrangeSamlauthUtilities::addString($authn_context_el, 'urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AuthnContextDeclRef', $this->authnContextDeclRef);
    }

    MiniOrangeSamlauthUtilities::addStrings($authn_context_el, 'urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AuthenticatingAuthority', FALSE, $this->AuthenticatingAuthority);
  }

  /**
   * Add an AttributeStatement-node to the assertion.
   *
   * @param DOMElement $root
   *          The assertion element we should add the subject to.
   */
  private function addAttributeStatement(DOMElement $root) {
    if (empty($this->attributes)) {
      return;
    }

    $document = $root->ownerDocument;

    $attribute_statement = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AttributeStatement');
    $root->appendChild($attribute_statement);

    foreach ($this->attributes as $name => $values) {
      $attribute = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Attribute');
      $attribute_statement->appendChild($attribute);
      $attribute->setAttribute('Name', $name);

      if ($this->nameFormat !== 'urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified') {
        $attribute->setAttribute('NameFormat', $this->nameFormat);
      }

      foreach ($values as $value) {
        if (is_string($value)) {
          $type = 'xs:string';
        }
        elseif (is_int($value)) {
          $type = 'xs:integer';
        }
        else {
          $type = NULL;
        }

        $attribute_value = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AttributeValue');
        $attribute->appendChild($attribute_value);
        if ($type !== NULL) {
          $attribute_value->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', $type);
        }
        if (is_null($value)) {
          $attribute_value->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:nil', 'true');
        }

        if ($value instanceof DOMNodeList) {
          for ($i = 0; $i < $value->length; $i++) {
            $node = $document->importNode($value->item($i), TRUE);
            $attribute_value->appendChild($node);
          }
        }
        else {
          $attribute_value->appendChild($document->createTextNode($value));
        }
      }
    }
  }

  /**
   * Add an EncryptedAttribute Statement-node to the assertion.
   *
   * @param DOMElement $root
   *          The assertion element we should add the Encrypted Attribute Statement to.
   */
  private function addEncryptedAttributeStatement(DOMElement $root) {
    if ($this->requiredEncAttributes == FALSE) {
      return;
    }

    $document = $root->ownerDocument;

    $attribute_statement = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AttributeStatement');
    $root->appendChild($attribute_statement);

    foreach ($this->attributes as $name => $values) {
      $document2 = new DOMDocument();
      $attribute = $document2->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:Attribute');
      $attribute->setAttribute('Name', $name);
      $document2->appendChild($attribute);

      if ($this->nameFormat !== 'urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified') {
        $attribute->setAttribute('NameFormat', $this->nameFormat);
      }

      foreach ($values as $value) {
        if (is_string($value)) {
          $type = 'xs:string';
        }
        elseif (is_int($value)) {
          $type = 'xs:integer';
        }
        else {
          $type = NULL;
        }

        $attribute_value = $document2->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:AttributeValue');
        $attribute->appendChild($attribute_value);
        if ($type !== NULL) {
          $attribute_value->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', $type);
        }

        if ($value instanceof DOMNodeList) {
          for ($i = 0; $i < $value->length; $i++) {
            $node = $document2->importNode($value->item($i), TRUE);
            $attribute_value->appendChild($node);
          }
        }
        else {
          $attribute_value->appendChild($document2->createTextNode($value));
        }
      }
      /* Once the attribute nodes are built, the are encrypted */
      $enc_assert = new XMLSecEnc();
      $enc_assert->setNode($document2->documentElement);
      $enc_assert->type = 'http://www.w3.org/2001/04/xmlenc#Element';
      /*
       * Attributes are encrypted with a session key and this one with
       * $EncryptionKey
       */
      $symmetric_key = new XMLSecurityKey(XMLSecurityKey::AES256_CBC);
      $symmetric_key->generateSessionKey();
      $enc_assert->encryptKey($this->encryptionKey, $symmetric_key);
      $encr_node = $enc_assert->encryptNode($symmetric_key);

      $enc_attribute = $document->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'saml:EncryptedAttribute');
      $attribute_statement->appendChild($enc_attribute);
      $n = $document->importNode($encr_node, TRUE);
      $enc_attribute->appendChild($n);
    }
  }

}
