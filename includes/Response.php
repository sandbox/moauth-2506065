<?php
/**
 * @file
 * The Response.php file for the miniorange_samlauth module.
 *
 * This file is part of miniOrange SAML plugin.
 */

include 'Assertion.php';
/**
 * The MiniOrangeSamlauthResponse class.
 */
class MiniOrangeSamlauthResponse {
  /**
   * The assertions in this response.
   */
  private $assertions;

  /**
   * The destination URL in this response.
   */
  private $destination;

  /**
   * Constructor for SAML 2 response messages.
   *
   * @param DOMElement|NULL $xml
   *          The input message.
   */
  public function __construct(DOMElement $xml = NULL) {
    // parent::__construct('Response', $xml);
    $this->assertions = array();

    if ($xml === NULL) {
      return;
    }

    /* set the destination from saml response */
    if ($xml->hasAttribute('Destination')) {
      $this->destination = $xml->getAttribute('Destination');
    }

    for ($node = $xml->firstChild; $node !== NULL; $node = $node->nextSibling) {
      if ($node->namespaceURI !== 'urn:oasis:names:tc:SAML:2.0:assertion') {
        continue;
      }

      if ($node->localName === 'Assertion') {
        $this->assertions[] = new MiniOrangeSamlauthAssertion($node);
      }
      /*
       * elseif ($node->localName === 'EncryptedAssertion') {
       * $this->assertions[] = new SAML2_EncryptedAssertion($node);
       * }
       */
    }
  }

  /**
   * Retrieve the assertions in this response.
   */
  public function getAssertions() {
    return $this->assertions;
  }

  /**
   * Set the assertions that should be included in this response.
   */
  public function setAssertions(array $assertions) {
    $this->assertions = $assertions;
  }

  /**
   * The function getDestination.
   */
  public function getDestination() {
    return $this->destination;
  }

}
