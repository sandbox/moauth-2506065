<?php
/**
 * @file
 * The AuthnRequest.php file for the miniorange_samlauth module.
 *
 * @package miniOrange
 *
 * @license GNU/GPLv3
 *
 * @copyright Copyright 2015 miniOrange. All Rights Reserved.
 *
 * This file is part of miniOrange SAML plugin.
 */

include 'Utilities.php';
/**
 * The MiniOrangeSamlauthAuthnRequest class.
 */
class MiniOrangeSamlauthAuthnRequest {

  /**
   * The function initiateLogin.
   */
  public function initiateLogin() {
    // Setting the acs url.
    $acs_url = $GLOBALS['base_url'] . '/miniorange_samlauth/acs';
    $issuer = 'miniorange-drupal-authentication-plugin';
    $sso_url = variable_get('miniorange_samlauth_saml_idp_sso');
    $saml_request = MiniOrangeSamlauthUtilities::createAuthnRequest($acs_url, $issuer);

    $redirect = $sso_url . '?SAMLRequest=' . $saml_request;
    header('Location: ' . $redirect);
  }

}
