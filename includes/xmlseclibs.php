<?php

/**
 * @file
 * The xmlseclibs.php file for the miniorange_samlauth module.
 *
 * Copyright (c) 2007-2013, Robert Richards <rrichards@cdatazone.org>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Robert Richards nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Robert Richards <rrichards@cdatazone.org>
 *
 * @copyright 2007-2013 Robert Richards <rrichards@cdatazone.org>
 *
 * @license http://www.opensource.org/licenses/bsd-license.php  BSD License
 *
 * @version 1.3.1
 */

/**
 * Functions to generate simple cases of Exclusive Canonical XML - Callable function is C14NGeneral() i.e.: $canonical = C14NGeneral($domelement, TRUE);.
 */

/**
 * Helper function.
 */
function miniorange_samlauth_sort_and_add_attrs($element, $ar_atts) {
  $new_atts = array();
  foreach ($ar_atts as $attnode) {
    $new_atts[$attnode->nodeName] = $attnode;
  }
  ksort($new_atts);
  foreach ($new_atts as $attnode) {
    $element->setAttribute($attnode->nodeName, $attnode->nodeValue);
  }
}

/**
 * Helper function.
 */
function miniorange_samlauth_canonical($tree, $element, $withcomments) {
  if ($tree->nodeType != XML_DOCUMENT_NODE) {
    $dom = $tree->ownerDocument;
  }
  else {
    $dom = $tree;
  }
  if ($element->nodeType != XML_ELEMENT_NODE) {
    if ($element->nodeType == XML_DOCUMENT_NODE) {
      foreach ($element->childNodes as $node) {
        miniorange_samlauth_canonical($dom, $node, $withcomments);
      }
      return;
    }
    if ($element->nodeType == XML_COMMENT_NODE && !$withcomments) {
      return;
    }
    $tree->appendChild($dom->importNode($element, TRUE));
    return;
  }
  $ar_ns = array();
  if ($element->namespaceURI != "") {
    if ($element->prefix == "") {
      $el_copy = $dom->createElementNS($element->namespaceURI, $element->nodeName);
    }
    else {
      $prefix = $tree->lookupPrefix($element->namespaceURI);
      if ($prefix == $element->prefix) {
        $el_copy = $dom->createElementNS($element->namespaceURI, $element->nodeName);
      }
      else {
        $el_copy = $dom->createElement($element->nodeName);
        $ar_ns[$element->namespaceURI] = $element->prefix;
      }
    }
  }
  else {
    $el_copy = $dom->createElement($element->nodeName);
  }
  $tree->appendChild($el_copy);

  /* Create DOMXPath based on original document */
  $xpath = new DOMXPath($element->ownerDocument);

  /* Get namespaced attributes */
  $ar_atts = $xpath->query('attribute::*[namespace-uri(.) != ""]', $element);

  /* Create an array with namespace URIs as keys, and sort them */
  foreach ($ar_atts as $attnode) {
    if (array_key_exists($attnode->namespaceURI, $ar_ns) && ($ar_ns[$attnode->namespaceURI] == $attnode->prefix)) {
      continue;
    }
    $prefix = $tree->lookupPrefix($attnode->namespaceURI);
    if ($prefix != $attnode->prefix) {
      $ar_ns[$attnode->namespaceURI] = $attnode->prefix;
    }
    else {
      $ar_ns[$attnode->namespaceURI] = NULL;
    }
  }
  if (count($ar_ns) > 0) {
    asort($ar_ns);
  }

  /* Add namespace nodes */
  foreach ($ar_ns as $namespace_uri => $prefix) {
    if ($prefix != NULL) {
      $el_copy->setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" . $prefix, $namespace_uri);
    }
  }
  if (count($ar_ns) > 0) {
    ksort($ar_ns);
  }

  /* Get attributes not in a namespace, and then sort and add them */
  $ar_atts = $xpath->query('attribute::*[namespace-uri(.) = ""]', $element);
  miniorange_samlauth_sort_and_add_attrs($el_copy, $ar_atts);

  /* Loop through the URIs, and then sort and add attributes within that namespace */
  foreach ($ar_ns as $ns_uri => $prefix) {
    $ar_atts = $xpath->query('attribute::*[namespace-uri(.) = "' . $ns_uri . '"]', $element);
    miniorange_samlauth_sort_and_add_attrs($el_copy, $ar_atts);
  }

  foreach ($element->childNodes as $node) {
    miniorange_samlauth_canonical($el_copy, $node, $withcomments);
  }
}

/**
 * The function miniorange_samlauth_c14_ngeneral.
 */
function miniorange_samlauth_c14_ngeneral($element, $exclusive = FALSE, $withcomments = FALSE) {
  /* IF PHP 5.2+ then use built in canonical functionality */
  $php_version = explode('.', PHP_VERSION);
  if (($php_version[0] > 5) || ($php_version[0] == 5 && $php_version[1] >= 2)) {
    return $element->C14N($exclusive, $withcomments);
  }

  /* Must be element or document */
  if (!$element instanceof DOMElement && !$element instanceof DOMDocument) {
    return NULL;
  }
  /* Currently only exclusive XML is supported */
  if ($exclusive == FALSE) {
    throw new Exception("Only exclusive canonicalization is supported in this version of PHP");
  }

  $copy_doc = new DOMDocument();
  miniorange_samlauth_canonical($copy_doc, $element, $withcomments);
  return $copy_doc->saveXML($copy_doc->documentElement, LIBXML_NOEMPTYTAG);
}

/**
 * The XMLSecurityKey class.
 */
class XMLSecurityKey {
  const TRIPLEDES_CBC = 'http://www.w3.org/2001/04/xmlenc#tripledes-cbc';
  const AES128_CBC = 'http://www.w3.org/2001/04/xmlenc#aes128-cbc';
  const AES192_CBC = 'http://www.w3.org/2001/04/xmlenc#aes192-cbc';
  const AES256_CBC = 'http://www.w3.org/2001/04/xmlenc#aes256-cbc';
  const RSA_1_5 = 'http://www.w3.org/2001/04/xmlenc#rsa-1_5';
  const RSA_OAEP_MGF1P = 'http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p';
  const DSA_SHA1 = 'http://www.w3.org/2000/09/xmldsig#dsa-sha1';
  const RSA_SHA1 = 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';
  const RSA_SHA256 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
  const RSA_SHA384 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384';
  const RSA_SHA512 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512';
  private $cryptParams = array();
  public $type = 0;
  public $key = NULL;
  public $passphrase = "";
  public $iv = NULL;
  public $name = NULL;
  public $keyChain = NULL;
  public $isEncrypted = FALSE;
  public $encryptedCtx = NULL;
  public $guid = NULL;

  /**
   * This variable contains the certificate as a string if this key represents an X509-certificate.
   *
   * If this key doesn't represent a certificate, this will be NULL.
   */
  private $x509Certificate = NULL;

  /* This variable contains the certificate thunbprint if we have loaded an X509-certificate. */
  private $X509Thumbprint = NULL;

  /**
   * The constructor function.
   */
  public function __construct($type, $params = NULL) {
    srand();
    switch ($type) {
      case (XMLSecurityKey::TRIPLEDES_CBC):
        $this->cryptParams['library'] = 'mcrypt';
        $this->cryptParams['cipher'] = MCRYPT_TRIPLEDES;
        $this->cryptParams['mode'] = MCRYPT_MODE_CBC;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#tripledes-cbc';
        $this->cryptParams['keysize'] = 24;
        break;

      case (XMLSecurityKey::AES128_CBC):
        $this->cryptParams['library'] = 'mcrypt';
        $this->cryptParams['cipher'] = MCRYPT_RIJNDAEL_128;
        $this->cryptParams['mode'] = MCRYPT_MODE_CBC;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#aes128-cbc';
        $this->cryptParams['keysize'] = 16;
        break;

      case (XMLSecurityKey::AES192_CBC):
        $this->cryptParams['library'] = 'mcrypt';
        $this->cryptParams['cipher'] = MCRYPT_RIJNDAEL_128;
        $this->cryptParams['mode'] = MCRYPT_MODE_CBC;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#aes192-cbc';
        $this->cryptParams['keysize'] = 24;
        break;

      case (XMLSecurityKey::AES256_CBC):
        $this->cryptParams['library'] = 'mcrypt';
        $this->cryptParams['cipher'] = MCRYPT_RIJNDAEL_128;
        $this->cryptParams['mode'] = MCRYPT_MODE_CBC;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#aes256-cbc';
        $this->cryptParams['keysize'] = 32;
        break;

      case (XMLSecurityKey::RSA_1_5):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_PADDING;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#rsa-1_5';
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
        throw new Exception('Certificate "type" (private/public) must be passed via parameters');

      return;
      case (XMLSecurityKey::RSA_OAEP_MGF1P):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_OAEP_PADDING;
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p';
        $this->cryptParams['hash'] = NULL;
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
        throw new Exception('Certificate "type" (private/public) must be passed via parameters');

      return;
      case (XMLSecurityKey::RSA_SHA1):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['method'] = 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_PADDING;
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
        throw new Exception('Certificate "type" (private/public) must be passed via parameters');

      break;

      case (XMLSecurityKey::RSA_SHA256):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_PADDING;
        $this->cryptParams['digest'] = 'SHA256';
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
        throw new Exception('Certificate "type" (private/public) must be passed via parameters');

      break;

      case (XMLSecurityKey::RSA_SHA384):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_PADDING;
        $this->cryptParams['digest'] = 'SHA384';
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
      case (XMLSecurityKey::RSA_SHA512):
        $this->cryptParams['library'] = 'openssl';
        $this->cryptParams['method'] = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512';
        $this->cryptParams['padding'] = OPENSSL_PKCS1_PADDING;
        $this->cryptParams['digest'] = 'SHA512';
        if (is_array($params) && !empty($params['type'])) {
          if ($params['type'] == 'public' || $params['type'] == 'private') {
            $this->cryptParams['type'] = $params['type'];
            break;

          }
        }
      default:
        throw new Exception('Invalid Key Type');

      return;
    }
    $this->type = $type;
  }

  /**
   * Retrieve the key size for the symmetric encryption algorithm..
   *
   * If the key size is unknown, or this isn't a symmetric encryption algorithm,
   * NULL is returned.
   */
  public function getSymmetricKeySize() {
    if (!isset($this->cryptParams['keysize'])) {
      return NULL;
    }
    return $this->cryptParams['keysize'];
  }

  /**
   * The function generateSessionKey.
   */
  public function generateSessionKey() {
    if (!isset($this->cryptParams['keysize'])) {
      throw new Exception('Unknown key size for type "' . $this->type . '".');
    }
    $keysize = $this->cryptParams['keysize'];

    if (function_exists('openssl_random_pseudo_bytes')) {
      /* We have PHP >= 5.3 - use openssl to generate session key. */
      $key = openssl_random_pseudo_bytes($keysize);
    }
    else {
      /* Generating random key using iv generation routines. */
      $key = mcrypt_create_iv($keysize, MCRYPT_RAND);
    }

    if ($this->type === XMLSecurityKey::TRIPLEDES_CBC) {
      /* Make sure that the generated key has the proper parity bits set. */

      /* Mcrypt doesn't care about the parity bits, but others may care. */
      for ($i = 0; $i < strlen($key); $i++) {
        $byte = ord($key[$i]) & 0xfe;
        $parity = 1;
        for ($j = 1; $j < 8; $j++) {
          $parity ^= ($byte >> $j) & 1;
        }
        $byte |= $parity;
        $key[$i] = chr($byte);
      }
    }

    $this->key = $key;
    return $key;
  }

  /**
   * The function getRawThumbprint.
   */
  public static function getRawThumbprint($cert) {
    $ar_cert = explode("\n", $cert);
    $data = '';
    $in_data = FALSE;

    foreach ($ar_cert as $cur_data) {
      if (!$in_data) {
        if (strncmp($cur_data, '-----BEGIN CERTIFICATE', 22) == 0) {
          $in_data = TRUE;
        }
      }
      else {
        if (strncmp($cur_data, '-----END CERTIFICATE', 20) == 0) {
          $in_data = FALSE;
          break;
        }
        $data .= trim($cur_data);
      }
    }

    if (!empty($data)) {
      return strtolower(sha1(base64_decode($data)));
    }

    return NULL;
  }

  /**
   * The function loadKey.
   */
  public function loadKey($key, $is_file = FALSE, $is_cert = FALSE) {
    if ($is_file) {
      $this->key = file_get_contents($key);
    }
    else {
      $this->key = $key;
    }
    if ($is_cert) {
      $this->key = openssl_x509_read($this->key);
      openssl_x509_export($this->key, $str_cert);
      $this->x509Certificate = $str_cert;
      $this->key = $str_cert;
    }
    else {
      $this->x509Certificate = NULL;
    }
    if ($this->cryptParams['library'] == 'openssl') {
      if ($this->cryptParams['type'] == 'public') {
        if ($is_cert) {
          /* Load the thumbprint if this is an X509 certificate. */
          $this->X509Thumbprint = self::getRawThumbprint($this->key);
        }
        $this->key = openssl_get_publickey($this->key);
      }
      else {
        $this->key = openssl_get_privatekey($this->key, $this->passphrase);
      }
    }
    else {
      if ($this->cryptParams['cipher'] == MCRYPT_RIJNDAEL_128) {
        /* Check key length. */
        switch ($this->type) {
          case (XMLSecurityKey::AES256_CBC):
            if (strlen($this->key) < 25) {
              throw new Exception('Key must contain at least 25 characters for this cipher');
            }
            break;

          case (XMLSecurityKey::AES192_CBC):
            if (strlen($this->key) < 17) {
              throw new Exception('Key must contain at least 17 characters for this cipher');
            }
            break;

        }
      }
    }
  }

  /**
   * The fuction encryptMcrypt.
   */
  private function encryptMcrypt($data) {
    $td = mcrypt_module_open($this->cryptParams['cipher'], '', $this->cryptParams['mode'], '');
    $this->iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $this->key, $this->iv);
    if ($this->cryptParams['mode'] == MCRYPT_MODE_CBC) {
      $bs = mcrypt_enc_get_block_size($td);
      for ($datalen0 = $datalen = strlen($data); (($datalen % $bs) != ($bs - 1)); $datalen++) {
        $data .= chr(rand(1, 127));
      }
      $data .= chr($datalen - $datalen0 + 1);
    }
    $encrypted_data = $this->iv . mcrypt_generic($td, $data);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    return $encrypted_data;
  }

  /**
   * The function decryptMcrypt.
   */
  private function decryptMcrypt($data) {
    $td = mcrypt_module_open($this->cryptParams['cipher'], '', $this->cryptParams['mode'], '');
    $iv_length = mcrypt_enc_get_iv_size($td);

    $this->iv = substr($data, 0, $iv_length);
    $data = substr($data, $iv_length);

    mcrypt_generic_init($td, $this->key, $this->iv);
    $decrypted_data = mdecrypt_generic($td, $data);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    if ($this->cryptParams['mode'] == MCRYPT_MODE_CBC) {
      $data_len = strlen($decrypted_data);
      $padding_length = substr($decrypted_data, $data_len - 1, 1);
      $decrypted_data = substr($decrypted_data, 0, $data_len - ord($padding_length));
    }
    return $decrypted_data;
  }

  /**
   * The function encryptOpenSsl.
   */
  private function encryptOpenSsl($data) {
    if ($this->cryptParams['type'] == 'public') {
      if (!openssl_public_encrypt($data, $encrypted_data, $this->key, $this->cryptParams['padding'])) {
        throw new Exception('Failure encrypting Data');
        return;
      }
    }
    else {
      if (!openssl_private_encrypt($data, $encrypted_data, $this->key, $this->cryptParams['padding'])) {
        throw new Exception('Failure encrypting Data');
        return;
      }
    }
    return $encrypted_data;
  }

  /**
   * The function decryptOpenSsl.
   */
  private function decryptOpenSsl($data) {
    if ($this->cryptParams['type'] == 'public') {
      if (!openssl_public_decrypt($data, $decrypted, $this->key, $this->cryptParams['padding'])) {
        throw new Exception('Failure decrypting Data');
        return;
      }
    }
    else {
      if (!openssl_private_decrypt($data, $decrypted, $this->key, $this->cryptParams['padding'])) {
        throw new Exception('Failure decrypting Data');
        return;
      }
    }
    return $decrypted;
  }

  /**
   * The function signOpenSsl.
   */
  private function signOpenSsl($data) {
    $algo = OPENSSL_ALGO_SHA1;
    if (!empty($this->cryptParams['digest'])) {
      $algo = $this->cryptParams['digest'];
    }
    if (!openssl_sign($data, $signature, $this->key, $algo)) {
      throw new Exception('Failure Signing Data: ' . openssl_error_string() . ' - ' . $algo);
      return;
    }
    return $signature;
  }

  /**
   * The function verifyOpenSsl.
   */
  private function verifyOpenSsl($data, $signature) {
    $algo = OPENSSL_ALGO_SHA1;
    if (!empty($this->cryptParams['digest'])) {
      $algo = $this->cryptParams['digest'];
    }
    return openssl_verify($data, $signature, $this->key, $algo);
  }

  /**
   * The function encryptData.
   */
  public function encryptData($data) {
    switch ($this->cryptParams['library']) {
      case 'mcrypt':
        return $this->encryptMcrypt($data);

      break;

      case 'openssl':
        return $this->encryptOpenSsl($data);

      break;

    }
  }

  /**
   * The function decryptData.
   */
  public function decryptData($data) {
    switch ($this->cryptParams['library']) {
      case 'mcrypt':
        return $this->decryptMcrypt($data);

      break;

      case 'openssl':
        return $this->decryptOpenSsl($data);

      break;

    }
  }

  /**
   * The function signData.
   */
  public function signData($data) {
    switch ($this->cryptParams['library']) {
      case 'openssl':
        return $this->signOpenSsl($data);

      break;

    }
  }

  /**
   * The function verifySignature.
   */
  public function verifySignature($data, $signature) {
    switch ($this->cryptParams['library']) {
      case 'openssl':
        return $this->verifyOpenSsl($data, $signature);

      break;

    }
  }

  /**
   * The function getAlgorith.
   */
  public function getAlgorith() {
    return $this->cryptParams['method'];
  }

  /**
   * The function makeAsnSegment.
   */
  private static function makeAsnSegment($type, $string) {
    switch ($type) {
      case 0x02:
        if (ord($string) > 0x7f) {
          $string = chr(0) . $string;
        }
        break;

      case 0x03:
        $string = chr(0) . $string;
        break;

    }

    $length = strlen($string);

    if ($length < 128) {
      $output = sprintf("%c%c%s", $type, $length, $string);
    }
    else {
      if ($length < 0x0100) {
        $output = sprintf("%c%c%c%s", $type, 0x81, $length, $string);
      }
      else {
        if ($length < 0x010000) {
          $output = sprintf("%c%c%c%c%s", $type, 0x82, $length / 0x0100, $length % 0x0100, $string);
        }
        else {
          $output = NULL;
        }
      }
    }
    return ($output);
  }

  /**
   * Modulus and Exponent must already be base64 decoded.
   */
  private static function convertRsa($modulus, $exponent) {
    /* make an ASN public_key_info */
    $exponent_encoding = XMLSecurityKey::makeAsnSegment(0x02, $exponent);
    $modulus_encoding = XMLSecurityKey::makeAsnSegment(0x02, $modulus);
    $sequence_encoding = XMLSecurityKey::makeAsnSegment(0x30, $modulus_encoding . $exponent_encoding);
    $bitstring_encoding = XMLSecurityKey::makeAsnSegment(0x03, $sequence_encoding);
    $rsa_algorithm_identifier = pack("H*", "300D06092A864886F70D0101010500");
    $public_key_info = XMLSecurityKey::makeAsnSegment(0x30, $rsa_algorithm_identifier . $bitstring_encoding);

    /* encode the public_key_info in base64 and add PEM brackets */
    $public_key_info_base64 = base64_encode($public_key_info);
    $encoding = "-----BEGIN PUBLIC KEY-----\n";
    $offset = 0;
    while ($segment = substr($public_key_info_base64, $offset, 64)) {
      $encoding = $encoding . $segment . "\n";
      $offset += 64;
    }
    return $encoding . "-----END PUBLIC KEY-----\n";
  }

  /**
   * The function serializeKey.
   */
  public function serializeKey($parent) {
  }

  /**
   * Retrieve the X509 certificate this key represents.
   *
   * Will return the X509 certificate in PEM-format if this key represents an X509 certificate.
   */
  public function getX509Certificate() {
    return $this->x509Certificate;
  }

  /**
   * Get the thumbprint of this X509 certificate.
   *
   * Returns:
   * The thumbprint as a lowercase 40-character hexadecimal number, or NULL if this isn't a X509 certificate.
   */
  public function getX509Thumbprint() {
    return $this->X509Thumbprint;
  }

  /**
   * Create key from an EncryptedKey-element.
   */
  public static function fromEncryptedKeyElement(DOMElement $element) {
    $objenc = new XMLSecEnc();
    $objenc->setNode($element);
    if (!$obj_key = $objenc->locateKey()) {
      throw new Exception("Unable to locate algorithm for this Encrypted Key");
    }
    $obj_key->isEncrypted = TRUE;
    $obj_key->encryptedCtx = $objenc;
    XMLSecEnc::staticLocateKeyInfo($obj_key, $element);
    return $obj_key;
  }

}

/**
 * The XMLSecurityDSig class.
 */
class XMLSecurityDSig {

  const XMLDSIGNS = 'http://www.w3.org/2000/09/xmldsig#';
  const SHA1 = 'http://www.w3.org/2000/09/xmldsig#sha1';
  const SHA256 = 'http://www.w3.org/2001/04/xmlenc#sha256';
  const SHA384 = 'http://www.w3.org/2001/04/xmldsig-more#sha384';
  const SHA512 = 'http://www.w3.org/2001/04/xmlenc#sha512';
  const RIPEMD160 = 'http://www.w3.org/2001/04/xmlenc#ripemd160';
  const C14N = 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';
  const C14N_COMMENTS = 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments';
  const EXC_C14N = 'http://www.w3.org/2001/10/xml-exc-c14n#';
  const EXC_C14N_COMMENTS = 'http://www.w3.org/2001/10/xml-exc-c14n#WithComments';
  const TEMPLATE = '<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
  <ds:SignedInfo>
    <ds:SignatureMethod />
  </ds:SignedInfo>
</ds:Signature>';
  public $sigNode = NULL;
  public $idKeys = array();
  public $idNS = array();
  private $signedInfo = NULL;
  private $xPathCtx = NULL;
  private $canonicalMethod = NULL;
  private $prefix = 'ds';
  private $searchpfx = 'secdsig';

  /**
   * This variable contains an associative array of validated nodes.
   */
  private $validatedNodes = NULL;

  /**
   * The constructor function.
   */
  public function __construct() {
    $sigdoc = new DOMDocument();
    $sigdoc->loadXML(XMLSecurityDSig::TEMPLATE);
    $this->sigNode = $sigdoc->documentElement;
  }

  /**
   * The function resetxPathObj.
   */
  private function resetxPathObj() {
    $this->xPathCtx = NULL;
  }

  /**
   * The function getxPathObj.
   */
  private function getxPathObj() {
    if (empty($this->xPathCtx) && !empty($this->sigNode)) {
      $xpath = new DOMXPath($this->sigNode->ownerDocument);
      $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
      $this->xPathCtx = $xpath;
    }
    return $this->xPathCtx;
  }

  /**
   * The function generateGuid.
   */
  private static function generateGuid($prefix = 'pfx') {
    $uuid = md5(uniqid(rand(), TRUE));
    $guid = $prefix . substr($uuid, 0, 8) . "-" . substr($uuid, 8, 4) . "-" . substr($uuid, 12, 4) . "-" . substr($uuid, 16, 4) . "-" . substr($uuid, 20, 12);
    return $guid;
  }

  /**
   * The function locateSignature.
   */
  public function locateSignature($obj_doc) {
    if ($obj_doc instanceof DOMDocument) {
      $doc = $obj_doc;
    }
    else {
      $doc = $obj_doc->ownerDocument;
    }
    if ($doc) {
      $xpath = new DOMXPath($doc);
      $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
      $query = ".//secdsig:Signature";
      $nodeset = $xpath->query($query, $obj_doc);
      $this->sigNode = $nodeset->item(0);
      return $this->sigNode;
    }
    return NULL;
  }

  /**
   * The function createNewSignNode.
   */
  public function createNewSignNode($name, $value = NULL) {
    $doc = $this->sigNode->ownerDocument;
    if (!is_null($value)) {
      $node = $doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, $this->prefix . ':' . $name, $value);
    }
    else {
      $node = $doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, $this->prefix . ':' . $name);
    }
    return $node;
  }

  /**
   * The function setCanonicalMethod.
   */
  public function setCanonicalMethod($method) {
    switch ($method) {
      case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315':
      case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments':
      case 'http://www.w3.org/2001/10/xml-exc-c14n#':
      case 'http://www.w3.org/2001/10/xml-exc-c14n#WithComments':
        $this->canonicalMethod = $method;
        break;

      default:
        throw new Exception('Invalid Canonical Method');
    }
    if ($xpath = $this->getxPathObj()) {
      $query = './' . $this->searchpfx . ':SignedInfo';
      $nodeset = $xpath->query($query, $this->sigNode);
      if ($sinfo = $nodeset->item(0)) {
        $query = './' . $this->searchpfx . 'CanonicalizationMethod';
        $nodeset = $xpath->query($query, $sinfo);
        if (!($canon_node = $nodeset->item(0))) {
          $canon_node = $this->createNewSignNode('CanonicalizationMethod');
          $sinfo->insertBefore($canon_node, $sinfo->firstChild);
        }
        $canon_node->setAttribute('Algorithm', $this->canonicalMethod);
      }
    }
  }

  /**
   * The function canonicalizeData.
   */
  private function canonicalizeData($node, $canonicalmethod, $ar_xpath = NULL, $prefix_list = NULL) {
    $exclusive = FALSE;
    $with_comments = FALSE;
    switch ($canonicalmethod) {
      case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315':
        $exclusive = FALSE;
        $with_comments = FALSE;
        break;

      case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments':
        $with_comments = TRUE;
        break;

      case 'http://www.w3.org/2001/10/xml-exc-c14n#':
        $exclusive = TRUE;
        break;

      case 'http://www.w3.org/2001/10/xml-exc-c14n#WithComments':
        $exclusive = TRUE;
        $with_comments = TRUE;
        break;

    }
    /* Support PHP versions < 5.2 not containing C14N methods in DOM extension. */
    $php_version = explode('.', PHP_VERSION);
    if (($php_version[0] < 5) || ($php_version[0] == 5 && $php_version[1] < 2)) {
      if (!is_null($ar_xpath)) {
        throw new Exception("PHP 5.2.0 or higher is required to perform XPath Transformations");
      }
      return miniorange_samlauth_c14_ngeneral($node, $exclusive, $with_comments);
    }
    $element = $node;
    if ($node instanceof DOMNode && $node->ownerDocument !== NULL && $node->isSameNode($node->ownerDocument->documentElement)) {
      $element = $node->ownerDocument;
    }
    return $element->C14N($exclusive, $with_comments, $ar_xpath, $prefix_list);
  }

  /**
   * The function canonicalizeSignedInfo.
   */
  public function canonicalizeSignedInfo() {
    $doc = $this->sigNode->ownerDocument;
    $canonicalmethod = NULL;
    if ($doc) {
      $xpath = $this->getxPathObj();
      $query = "./secdsig:SignedInfo";
      $nodeset = $xpath->query($query, $this->sigNode);
      if ($sign_info_node = $nodeset->item(0)) {
        $query = "./secdsig:CanonicalizationMethod";
        $nodeset = $xpath->query($query, $sign_info_node);
        if ($canon_node = $nodeset->item(0)) {
          $canonicalmethod = $canon_node->getAttribute('Algorithm');
        }
        $this->signedInfo = $this->canonicalizeData($sign_info_node, $canonicalmethod);
        return $this->signedInfo;
      }
    }
    return NULL;
  }

  /**
   * The function calculateDigest.
   */
  public function calculateDigest($digest_algorithm, $data) {
    switch ($digest_algorithm) {
      case XMLSecurityDSig::SHA1:
        $alg = 'sha1';
        break;

      case XMLSecurityDSig::SHA256:
        $alg = 'sha256';
        break;

      case XMLSecurityDSig::SHA384:
        $alg = 'sha384';
        break;

      case XMLSecurityDSig::SHA512:
        $alg = 'sha512';
        break;

      case XMLSecurityDSig::RIPEMD160:
        $alg = 'ripemd160';
        break;

      default:
        throw new Exception("Cannot validate digest: Unsupported Algorith <$digest_algorithm>");
    }
    if (function_exists('hash')) {
      return base64_encode(hash($alg, $data, TRUE));
    }
    elseif (function_exists('mhash')) {
      $alg = "MHASH_" . strtoupper($alg);
      return base64_encode(mhash(constant($alg), $data));
    }
    elseif ($alg === 'sha1') {
      return base64_encode(sha1($data, TRUE));
    }
    else {
      throw new Exception('xmlseclibs is unable to calculate a digest. Maybe you need the mhash library?');
    }
  }

  /**
   * The function validateDigest.
   */
  public function validateDigest($ref_node, $data) {
    $xpath = new DOMXPath($ref_node->ownerDocument);
    $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
    $query = 'string(./secdsig:DigestMethod/@Algorithm)';
    $digest_algorithm = $xpath->evaluate($query, $ref_node);
    $dig_value = $this->calculateDigest($digest_algorithm, $data);
    $query = 'string(./secdsig:DigestValue)';
    $digest_value = $xpath->evaluate($query, $ref_node);
    return ($dig_value == $digest_value);
  }

  /**
   * The function processTransforms.
   */
  public function processTransforms($ref_node, $obj_data, $include_comment_nodes = TRUE) {
    $data = $obj_data;
    $xpath = new DOMXPath($ref_node->ownerDocument);
    $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
    $query = './secdsig:Transforms/secdsig:Transform';
    $nodelist = $xpath->query($query, $ref_node);
    $canonical_method = 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';
    $ar_xpath = NULL;
    $prefix_list = NULL;
    foreach ($nodelist as $transform) {
      $algorithm = $transform->getAttribute("Algorithm");
      switch ($algorithm) {
        case 'http://www.w3.org/2001/10/xml-exc-c14n#':
        case 'http://www.w3.org/2001/10/xml-exc-c14n#WithComments':

          if (!$include_comment_nodes) {
            /*
             * We remove comment nodes by forcing it to use a canonicalization without comments.
             */
            $canonical_method = 'http://www.w3.org/2001/10/xml-exc-c14n#';
          }
          else {
            $canonical_method = $algorithm;
          }

          $node = $transform->firstChild;
          while ($node) {
            if ($node->localName == 'InclusiveNamespaces') {
              if ($pfx = $node->getAttribute('PrefixList')) {
                $arpfx = array();
                $pfxlist = explode(" ", $pfx);
                foreach ($pfxlist as $pfx) {
                  $val = trim($pfx);
                  if (!empty($val)) {
                    $arpfx[] = $val;
                  }
                }
                if (count($arpfx) > 0) {
                  $prefix_list = $arpfx;
                }
              }
              break;

            }
            $node = $node->nextSibling;
          }
          break;

        case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315':
        case 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments':
          if (!$include_comment_nodes) {
            /*
             * We remove comment nodes by forcing it to use a canonicalization without comments.
             */
            $canonical_method = 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315';
          }
          else {
            $canonical_method = $algorithm;
          }

          break;

        case 'http://www.w3.org/TR/1999/REC-xpath-19991116':
          $node = $transform->firstChild;
          while ($node) {
            if ($node->localName == 'XPath') {
              $ar_xpath = array();
              $ar_xpath['query'] = '(.//. | .//@* | .//namespace::*)[' . $node->nodeValue . ']';
              $ar_xpath['namespaces'] = array();
              $nslist = $xpath->query('./namespace::*', $node);
              foreach ($nslist as $nsnode) {
                if ($nsnode->localName != "xml") {
                  $ar_xpath['namespaces'][$nsnode->localName] = $nsnode->nodeValue;
                }
              }
              break;

            }
            $node = $node->nextSibling;
          }
          break;

      }
    }
    if ($data instanceof DOMNode) {
      $data = $this->canonicalizeData($obj_data, $canonical_method, $ar_xpath, $prefix_list);
    }
    return $data;
  }

  /**
   * The function processRefNode.
   */
  public function processRefNode($ref_node) {
    $data_object = NULL;

    /*
     * Depending on the URI, we may not want to include comments in the result.
     *
     * See: http://www.w3.org/TR/xmldsig-core/#sec-ReferenceProcessingModel.
     */
    $include_comment_nodes = TRUE;

    if ($uri = $ref_node->getAttribute("URI")) {
      $ar_url = parse_url($uri);
      if (empty($ar_url['path'])) {
        if ($identifier = $ar_url['fragment']) {

          /*
           * This reference identifies a node with the given id by using a URI on the form "#identifier".
           *
           * This should not include comments.
           */
          $include_comment_nodes = FALSE;

          $x_path = new DOMXPath($ref_node->ownerDocument);
          if ($this->idNS && is_array($this->idNS)) {
            foreach ($this->idNS as $nspf => $ns) {
              $x_path->registerNamespace($nspf, $ns);
            }
          }
          $id_list = '@Id="' . $identifier . '"';
          if (is_array($this->idKeys)) {
            foreach ($this->idKeys as $id_key) {
              $id_list .= " or @$id_key='$identifier'";
            }
          }
          $query = '//*[' . $id_list . ']';
          $data_object = $x_path->query($query)->item(0);
        }
        else {
          $data_object = $ref_node->ownerDocument;
        }
      }
      else {
        $data_object = file_get_contents($ar_url);
      }
    }
    else {
      /*
       * This reference identifies the root node with an empty URI.
       *
       * This should not include comments.
       */
      $include_comment_nodes = FALSE;

      $data_object = $ref_node->ownerDocument;
    }
    $data = $this->processTransforms($ref_node, $data_object, $include_comment_nodes);
    if (!$this->validateDigest($ref_node, $data)) {
      return FALSE;
    }

    if ($data_object instanceof DOMNode) {
      /* Add this node to the list of validated nodes. */
      if (!empty($identifier)) {
        $this->validatedNodes[$identifier] = $data_object;
      }
      else {
        $this->validatedNodes[] = $data_object;
      }
    }

    return TRUE;
  }

  /**
   * The function getRefNodeId.
   */
  public function getRefNodeId($ref_node) {
    if ($uri = $ref_node->getAttribute("URI")) {
      $ar_url = parse_url($uri);
      if (empty($ar_url['path'])) {
        if ($identifier = $ar_url['fragment']) {
          return $identifier;
        }
      }
    }
    return NULL;
  }

  /**
   * The function getRefIds.
   */
  public function getRefIds() {
    $refids = array();
    $doc = $this->sigNode->ownerDocument;

    $xpath = $this->getxPathObj();
    $query = "./secdsig:SignedInfo/secdsig:Reference";
    $nodeset = $xpath->query($query, $this->sigNode);
    if ($nodeset->length == 0) {
      throw new Exception("Reference nodes not found");
    }
    foreach ($nodeset as $ref_node) {
      $refids[] = $this->getRefNodeId($ref_node);
    }
    return $refids;
  }

  /**
   * The function validateReference.
   */
  public function validateReference() {
    $doc = $this->sigNode->ownerDocument;
    if (!$doc->isSameNode($this->sigNode)) {
      $this->sigNode->parentNode->removeChild($this->sigNode);
    }
    $xpath = $this->getxPathObj();
    $query = "./secdsig:SignedInfo/secdsig:Reference";
    $nodeset = $xpath->query($query, $this->sigNode);
    if ($nodeset->length == 0) {
      throw new Exception("Reference nodes not found");
    }

    /* Initialize/reset the list of validated nodes. */
    $this->validatedNodes = array();

    foreach ($nodeset as $ref_node) {
      if (!$this->processRefNode($ref_node)) {
        /* Clear the list of validated nodes. */
        $this->validatedNodes = NULL;
        throw new Exception("Reference validation failed");
      }
    }
    return TRUE;
  }

  /**
   * The function addRefInternal.
   */
  private function addRefInternal($sinfo_node, $node, $algorithm, $ar_transforms = NULL, $options = NULL) {
    $prefix = NULL;
    $prefix_ns = NULL;
    $id_name = 'Id';
    $overwrite_id = TRUE;
    $force_uri = FALSE;

    if (is_array($options)) {
      $prefix = empty($options['prefix']) ? NULL : $options['prefix'];
      $prefix_ns = empty($options['prefix_ns']) ? NULL : $options['prefix_ns'];
      $id_name = empty($options['id_name']) ? 'Id' : $options['id_name'];
      $overwrite_id = !isset($options['overwrite']) ? TRUE : (bool) $options['overwrite'];
      $force_uri = !isset($options['force_uri']) ? FALSE : (bool) $options['force_uri'];
    }

    $attname = $id_name;
    if (!empty($prefix)) {
      $attname = $prefix . ':' . $attname;
    }

    $ref_node = $this->createNewSignNode('Reference');
    $sinfo_node->appendChild($ref_node);

    if (!$node instanceof DOMDocument) {
      $uri = NULL;
      if (!$overwrite_id) {
        $uri = $node->getAttributeNS($prefix_ns, $id_name);
      }
      if (empty($uri)) {
        $uri = XMLSecurityDSig::generateGuid();
        $node->setAttributeNS($prefix_ns, $attname, $uri);
      }
      $ref_node->setAttribute("URI", '#' . $uri);
    }
    elseif ($force_uri) {
      $ref_node->setAttribute("URI", '');
    }

    $trans_nodes = $this->createNewSignNode('Transforms');
    $ref_node->appendChild($trans_nodes);

    if (is_array($ar_transforms)) {
      foreach ($ar_transforms as $transform) {
        $trans_node = $this->createNewSignNode('Transform');
        $trans_nodes->appendChild($trans_node);
        if (is_array($transform) && (!empty($transform['http://www.w3.org/TR/1999/REC-xpath-19991116'])) && (!empty($transform['http://www.w3.org/TR/1999/REC-xpath-19991116']['query']))) {
          $trans_node->setAttribute('Algorithm', 'http://www.w3.org/TR/1999/REC-xpath-19991116');
          $xpath_node = $this->createNewSignNode('XPath', $transform['http://www.w3.org/TR/1999/REC-xpath-19991116']['query']);
          $trans_node->appendChild($xpath_node);
          if (!empty($transform['http://www.w3.org/TR/1999/REC-xpath-19991116']['namespaces'])) {
            foreach ($transform['http://www.w3.org/TR/1999/REC-xpath-19991116']['namespaces'] as $prefix => $namespace) {
              $xpath_node->setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:$prefix", $namespace);
            }
          }
        }
        else {
          $trans_node->setAttribute('Algorithm', $transform);
        }
      }
    }
    elseif (!empty($this->canonicalMethod)) {
      $trans_node = $this->createNewSignNode('Transform');
      $trans_nodes->appendChild($trans_node);
      $trans_node->setAttribute('Algorithm', $this->canonicalMethod);
    }

    $canonical_data = $this->processTransforms($ref_node, $node);
    $dig_value = $this->calculateDigest($algorithm, $canonical_data);

    $digest_method = $this->createNewSignNode('DigestMethod');
    $ref_node->appendChild($digest_method);
    $digest_method->setAttribute('Algorithm', $algorithm);

    $digest_value = $this->createNewSignNode('DigestValue', $dig_value);
    $ref_node->appendChild($digest_value);
  }

  /**
   * The function addReference.
   */
  public function addReference($node, $algorithm, $ar_transforms = NULL, $options = NULL) {
    if ($xpath = $this->getxPathObj()) {
      $query = "./secdsig:SignedInfo";
      $nodeset = $xpath->query($query, $this->sigNode);
      if ($s_info = $nodeset->item(0)) {
        $this->addRefInternal($s_info, $node, $algorithm, $ar_transforms, $options);
      }
    }
  }

  /**
   * The function addReferenceList.
   */
  public function addReferenceList($ar_nodes, $algorithm, $ar_transforms = NULL, $options = NULL) {
    if ($xpath = $this->getxPathObj()) {
      $query = "./secdsig:SignedInfo";
      $nodeset = $xpath->query($query, $this->sigNode);
      if ($s_info = $nodeset->item(0)) {
        foreach ($ar_nodes as $node) {
          $this->addRefInternal($s_info, $node, $algorithm, $ar_transforms, $options);
        }
      }
    }
  }

  /**
   * The function addObject.
   */
  public function addObject($data, $mimetype = NULL, $encoding = NULL) {
    $obj_node = $this->createNewSignNode('Object');
    $this->sigNode->appendChild($obj_node);
    if (!empty($mimetype)) {
      $obj_node->setAtribute('MimeType', $mimetype);
    }
    if (!empty($encoding)) {
      $obj_node->setAttribute('Encoding', $encoding);
    }

    if ($data instanceof DOMElement) {
      $new_data = $this->sigNode->ownerDocument->importNode($data, TRUE);
    }
    else {
      $new_data = $this->sigNode->ownerDocument->createTextNode($data);
    }
    $obj_node->appendChild($new_data);

    return $obj_node;
  }

  /**
   * The function locateKey.
   */
  public function locateKey($node = NULL) {
    if (empty($node)) {
      $node = $this->sigNode;
    }
    if (!$node instanceof DOMNode) {
      return NULL;
    }
    if ($doc = $node->ownerDocument) {
      $xpath = new DOMXPath($doc);
      $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
      $query = "string(./secdsig:SignedInfo/secdsig:SignatureMethod/@Algorithm)";
      $algorithm = $xpath->evaluate($query, $node);
      if ($algorithm) {
        try {
          $obj_key = new XMLSecurityKey($algorithm, array('type' => 'public'));
        }
        catch (Exception $e) {
          return NULL;
        }
        return $obj_key;
      }
    }
    return NULL;
  }

  /**
   * The function verify.
   */
  public function verify($obj_key) {
    $doc = $this->sigNode->ownerDocument;
    $xpath = new DOMXPath($doc);
    $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
    $query = "string(./secdsig:SignatureValue)";
    $sig_value = $xpath->evaluate($query, $this->sigNode);
    if (empty($sig_value)) {
      throw new Exception("Unable to locate SignatureValue");
    }
    return $obj_key->verifySignature($this->signedInfo, base64_decode($sig_value));
  }

  /**
   * The function signData.
   */
  public function signData($obj_key, $data) {
    return $obj_key->signData($data);
  }

  /**
   * The function sign.
   */
  public function sign($obj_key, $append_to_node = NULL) {
    // If we have a parent node append it now so C14N properly works.
    if ($append_to_node != NULL) {
      $this->resetxPathObj();
      $this->appendSignature($append_to_node);
      $this->sigNode = $append_to_node->lastChild;
    }
    if ($xpath = $this->getxPathObj()) {
      $query = "./secdsig:SignedInfo";
      $nodeset = $xpath->query($query, $this->sigNode);
      if ($s_info = $nodeset->item(0)) {
        $query = "./secdsig:SignatureMethod";
        $nodeset = $xpath->query($query, $s_info);
        $s_method = $nodeset->item(0);
        $s_method->setAttribute('Algorithm', $obj_key->type);
        $data = $this->canonicalizeData($s_info, $this->canonicalMethod);
        $sig_value = base64_encode($this->signData($obj_key, $data));
        $sig_value_node = $this->createNewSignNode('SignatureValue', $sig_value);
        if ($info_sibling = $s_info->nextSibling) {
          $info_sibling->parentNode->insertBefore($sig_value_node, $info_sibling);
        }
        else {
          $this->sigNode->appendChild($sig_value_node);
        }
      }
    }
  }

  /**
   * The function appendCert.
   */
  public function appendCert() {
  }

  /**
   * The function appendKey.
   */
  public function appendKey($obj_key, $parent = NULL) {
    $obj_key->serializeKey($parent);
  }

  /**
   * This function inserts the signature element.
   *
   * The signature element will be appended to the element, unless $before_node is specified.
   *
   * If $before_node is specified, the signature element will be inserted as the last element before $before_node.
   */
  public function insertSignature($node, $before_node = NULL) {
    $document = $node->ownerDocument;
    $signature_element = $document->importNode($this->sigNode, TRUE);

    if ($before_node == NULL) {
      return $node->insertBefore($signature_element);
    }
    else {
      return $node->insertBefore($signature_element, $before_node);
    }
  }

  /**
   * The function appendSignature.
   */
  public function appendSignature($parent_node, $insert_before = FALSE) {
    $before_node = $insert_before ? $parent_node->firstChild : NULL;
    return $this->insertSignature($parent_node, $before_node);
  }

  /**
   * The function get509Xcert.
   */
  private static function get509Xcert($cert, $is_pem_format = TRUE) {
    $certs = XMLSecurityDSig::staticGet509Xcerts($cert, $is_pem_format);
    if (!empty($certs)) {
      return $certs[0];
    }
    return '';
  }

  /**
   * The function staticGet509Xcerts.
   */
  private static function staticGet509Xcerts($certs, $is_pem_format = TRUE) {
    if ($is_pem_format) {
      $data = '';
      $certlist = array();
      $ar_cert = explode("\n", $certs);
      $in_data = FALSE;
      foreach ($ar_cert as $cur_data) {
        if (!$in_data) {
          if (strncmp($cur_data, '-----BEGIN CERTIFICATE', 22) == 0) {
            $in_data = TRUE;
          }
        }
        else {
          if (strncmp($cur_data, '-----END CERTIFICATE', 20) == 0) {
            $in_data = FALSE;
            $certlist[] = $data;
            $data = '';
            continue;
          }
          $data .= trim($cur_data);
        }
      }
      return $certlist;
    }
    else {
      return array($certs);
    }
  }

  /**
   * The function staticAdd509Cert.
   */
  private static function staticAdd509Cert($parent_ref, $cert, $is_pem_format = TRUE, $is_url = FALSE, $xpath = NULL, $options = NULL) {
    if ($is_url) {
      $cert = file_get_contents($cert);
    }
    if (!$parent_ref instanceof DOMElement) {
      throw new Exception('Invalid parent Node parameter');
    }
    $base_doc = $parent_ref->ownerDocument;

    if (empty($xpath)) {
      $xpath = new DOMXPath($parent_ref->ownerDocument);
      $xpath->registerNamespace('secdsig', XMLSecurityDSig::XMLDSIGNS);
    }

    $query = "./secdsig:KeyInfo";
    $nodeset = $xpath->query($query, $parent_ref);
    $key_info = $nodeset->item(0);
    if (!$key_info) {
      $inserted = FALSE;
      $key_info = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:KeyInfo');

      $query = "./secdsig:Object";
      $nodeset = $xpath->query($query, $parent_ref);
      if ($s_object = $nodeset->item(0)) {
        $s_object->parentNode->insertBefore($key_info, $s_object);
        $inserted = TRUE;
      }

      if (!$inserted) {
        $parent_ref->appendChild($key_info);
      }
    }

    // Add all certs if there are more than one.
    $certs = XMLSecurityDSig::staticGet509Xcerts($cert, $is_pem_format);

    // Attach X509 data node.
    $x509_data_node = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:X509Data');
    $key_info->appendChild($x509_data_node);

    $issuer_serial = FALSE;

    if (is_array($options)) {
      if (!empty($options['issuerSerial'])) {
        $issuer_serial = TRUE;
      }
    }

    // Attach all certificate nodes and any additional data.
    foreach ($certs as $x509_cert) {
      if ($issuer_serial) {
        if ($cert_data = openssl_x509_parse("-----BEGIN CERTIFICATE-----\n" . chunk_split($x509_cert, 64, "\n") . "-----END CERTIFICATE-----\n")) {
          if ($issuer_serial && !empty($cert_data['issuer']) && !empty($cert_data['serialNumber'])) {
            if (is_array($cert_data['issuer'])) {
              $parts = array();
              foreach ($cert_data['issuer'] as $key => $value) {
                array_unshift($parts, "$key=$value" . $issuer);
              }
              $issuer_name = implode(',', $parts);
            }
            else {
              $issuer_name = $cert_data['issuer'];
            }

            $x509_issuer_node = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:X509IssuerSerial');
            $x509_data_node->appendChild($x509_issuer_node);

            $x509_node = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:X509IssuerName', $issuer_name);
            $x509_issuer_node->appendChild($x509_node);
            $x509_node = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:X509SerialNumber', $cert_data['serialNumber']);
            $x509_issuer_node->appendChild($x509_node);
          }
        }
      }
      $x509_cert_node = $base_doc->createElementNS(XMLSecurityDSig::XMLDSIGNS, 'ds:X509Certificate', $x509_cert);
      $x509_data_node->appendChild($x509_cert_node);
    }
  }

  /**
   * The function add509Cert.
   */
  public function add509Cert($cert, $is_pem_format = TRUE, $is_url = FALSE, $options = NULL) {
    if ($xpath = $this->getxPathObj()) {
      self::staticAdd509Cert($this->sigNode, $cert, $is_pem_format, $is_url, $xpath, $options);
    }
  }

  /**
   * This function retrieves an associative array of the validated nodes.
   *
   * The array will contain the id of the referenced node as the key and the node itself as the value.
   *
   * Returns:
   * An associative array of validated nodes or NULL if no nodes have been validated.
   */
  public function getValidatedNodes() {
    return $this->validatedNodes;
  }

}

/**
 * The XMLSecEnc class.
 */
class XMLSecEnc {

  const TEMPLATE = "<xenc:EncryptedData xmlns:xenc='http://www.w3.org/2001/04/xmlenc#'>
   <xenc:CipherData>
      <xenc:CipherValue></xenc:CipherValue>
   </xenc:CipherData>
</xenc:EncryptedData>";
  const ELEMENT = 'http://www.w3.org/2001/04/xmlenc#Element';
  const CONTENT = 'http://www.w3.org/2001/04/xmlenc#Content';
  const URI = 3;
  const XMLENCNS = 'http://www.w3.org/2001/04/xmlenc#';
  private $encdoc = NULL;
  private $rawNode = NULL;
  public $type = NULL;
  public $encKey = NULL;
  private $references = array();

  /**
   * The constructor function.
   */
  public function __construct() {
    $this->resetTemplate();
  }

  /**
   * The function resetTemplate.
   */
  private function resetTemplate() {
    $this->encdoc = new DOMDocument();
    $this->encdoc->loadXML(XMLSecEnc::template);
  }

  /**
   * The function addReference.
   */
  public function addReference($name, $node, $type) {
    if (!$node instanceof DOMNode) {
      throw new Exception('$node is not of type DOMNode');
    }
    $curencdoc = $this->encdoc;
    $this->resetTemplate();
    $encdoc = $this->encdoc;
    $this->encdoc = $curencdoc;
    $refuri = XMLSecurityDSig::generateGuid();
    $element = $encdoc->documentElement;
    $element->setAttribute("Id", $refuri);
    $this->references[$name] = array(
      "node" => $node,
      "type" => $type,
      "encnode" => $encdoc,
      "refuri" => $refuri,
    );
  }

  /**
   * The function setNode.
   */
  public function setNode($node) {
    $this->rawNode = $node;
  }

  /**
   * Encrypt the selected node with the given key.
   */
  public function encryptNode($obj_key, $replace = TRUE) {
    $data = '';
    if (empty($this->rawNode)) {
      throw new Exception('Node to encrypt has not been set');
    }
    if (!$obj_key instanceof XMLSecurityKey) {
      throw new Exception('Invalid Key');
    }
    $doc = $this->rawNode->ownerDocument;
    $x_path = new DOMXPath($this->encdoc);
    $obj_list = $x_path->query('/xenc:EncryptedData/xenc:CipherData/xenc:CipherValue');
    $cipher_value = $obj_list->item(0);
    if ($cipher_value == NULL) {
      throw new Exception('Error locating CipherValue element within template');
    }
    switch ($this->type) {
      case (XMLSecEnc::ELEMENT):
        $data = $doc->saveXML($this->rawNode);
        $this->encdoc->documentElement->setAttribute('Type', XMLSecEnc::ELEMENT);
        break;

      case (XMLSecEnc::CONTENT):
        $children = $this->rawNode->childNodes;
        foreach ($children as $child) {
          $data .= $doc->saveXML($child);
        }
        $this->encdoc->documentElement->setAttribute('Type', XMLSecEnc::CONTENT);
        break;

      default:
        throw new Exception('Type is currently not supported');

    }

    $enc_method = $this->encdoc->documentElement->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:EncryptionMethod'));
    $enc_method->setAttribute('Algorithm', $obj_key->getAlgorith());
    $cipher_value->parentNode->parentNode->insertBefore($enc_method, $cipher_value->parentNode->parentNode->firstChild);

    $str_encrypt = base64_encode($obj_key->encryptData($data));
    $value = $this->encdoc->createTextNode($str_encrypt);
    $cipher_value->appendChild($value);

    if ($replace) {
      switch ($this->type) {
        case (XMLSecEnc::ELEMENT):
          if ($this->rawNode->nodeType == XML_DOCUMENT_NODE) {
            return $this->encdoc;
          }
          $import_enc = $this->rawNode->ownerDocument->importNode($this->encdoc->documentElement, TRUE);
          $this->rawNode->parentNode->replaceChild($import_enc, $this->rawNode);
          return $import_enc;

        break;

        case (XMLSecEnc::CONTENT):
          $import_enc = $this->rawNode->ownerDocument->importNode($this->encdoc->documentElement, TRUE);
          while ($this->rawNode->firstChild) {
            $this->rawNode->removeChild($this->rawNode->firstChild);
          }
          $this->rawNode->appendChild($import_enc);
          return $import_enc;

        break;

      }
    }
    else {
      return $this->encdoc->documentElement;
    }
  }

  /**
   * The function encryptReferences.
   */
  public function encryptReferences($obj_key) {
    $cur_raw_node = $this->rawNode;
    $cur_type = $this->type;
    foreach ($this->references as $name => $reference) {
      $this->encdoc = $reference["encnode"];
      $this->rawNode = $reference["node"];
      $this->type = $reference["type"];
      try {
        $enc_node = $this->encryptNode($obj_key);
        $this->references[$name]["encnode"] = $enc_node;
      }
      catch (Exception $e) {
        $this->rawNode = $cur_raw_node;
        $this->type = $cur_type;
        throw $e;
      }
    }
    $this->rawNode = $cur_raw_node;
    $this->type = $cur_type;
  }

  /**
   * Retrieve the CipherValue text from this encrypted node.
   */
  public function getCipherValue() {
    if (empty($this->rawNode)) {
      throw new Exception('Node to decrypt has not been set');
    }

    $doc = $this->rawNode->ownerDocument;
    $x_path = new DOMXPath($doc);
    $x_path->registerNamespace('xmlencr', XMLSecEnc::XMLENCNS);
    /* Only handles embedded content right now and not a reference */
    $query = "./xmlencr:CipherData/xmlencr:CipherValue";
    $nodeset = $x_path->query($query, $this->rawNode);
    $node = $nodeset->item(0);

    if (!$node) {
      return NULL;
    }

    return base64_decode($node->nodeValue);
  }

  /**
   * Decrypt this encrypted node.
   *
   * The behaviour of this function depends on the value of $replace.
   *
   * If $replace is FALSE, we will return the decrypted data as a string.
   *
   * If $replace is TRUE, we will insert the decrypted element(s) into the document, and return the decrypted element(s).
   */
  public function decryptNode($obj_key, $replace = TRUE) {
    if (!$obj_key instanceof XMLSecurityKey) {
      throw new Exception('Invalid Key');
    }

    $encrypted_data = $this->getCipherValue();
    if ($encrypted_data) {
      $decrypted = $obj_key->decryptData($encrypted_data);
      if ($replace) {
        switch ($this->type) {
          case (XMLSecEnc::ELEMENT):
            $newdoc = new DOMDocument();
            $newdoc->loadXML($decrypted);
            if ($this->rawNode->nodeType == XML_DOCUMENT_NODE) {
              return $newdoc;
            }
            $import_enc = $this->rawNode->ownerDocument->importNode($newdoc->documentElement, TRUE);
            $this->rawNode->parentNode->replaceChild($import_enc, $this->rawNode);
            return $import_enc;

          break;

          case (XMLSecEnc::CONTENT):
            if ($this->rawNode->nodeType == XML_DOCUMENT_NODE) {
              $doc = $this->rawNode;
            }
            else {
              $doc = $this->rawNode->ownerDocument;
            }
            $new_frag = $doc->createDocumentFragment();
            $new_frag->appendXML($decrypted);
            $parent = $this->rawNode->parentNode;
            $parent->replaceChild($new_frag, $this->rawNode);
            return $parent;

          break;

          default:
            return $decrypted;
        }
      }
      else {
        return $decrypted;
      }
    }
    else {
      throw new Exception("Cannot locate encrypted data");
    }
  }

  /**
   * The function encryptKey.
   */
  public function encryptKey($src_key, $raw_key, $append = TRUE) {
    if ((!$src_key instanceof XMLSecurityKey) || (!$raw_key instanceof XMLSecurityKey)) {
      throw new Exception('Invalid Key');
    }
    $str_enc_key = base64_encode($src_key->encryptData($raw_key->key));
    $root = $this->encdoc->documentElement;
    $enc_key = $this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:EncryptedKey');
    if ($append) {
      $key_info = $root->insertBefore($this->encdoc->createElementNS('http://www.w3.org/2000/09/xmldsig#', 'dsig:KeyInfo'), $root->firstChild);
      $key_info->appendChild($enc_key);
    }
    else {
      $this->encKey = $enc_key;
    }
    $enc_method = $enc_key->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:EncryptionMethod'));
    $enc_method->setAttribute('Algorithm', $src_key->getAlgorith());
    if (!empty($src_key->name)) {
      $key_info = $enc_key->appendChild($this->encdoc->createElementNS('http://www.w3.org/2000/09/xmldsig#', 'dsig:KeyInfo'));
      $key_info->appendChild($this->encdoc->createElementNS('http://www.w3.org/2000/09/xmldsig#', 'dsig:KeyName', $src_key->name));
    }
    $cipher_data = $enc_key->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:CipherData'));
    $cipher_data->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:CipherValue', $str_enc_key));
    if (is_array($this->references) && count($this->references) > 0) {
      $ref_list = $enc_key->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:ReferenceList'));
      foreach ($this->references as $name => $reference) {
        $refuri = $reference["refuri"];
        $data_ref = $ref_list->appendChild($this->encdoc->createElementNS(XMLSecEnc::XMLENCNS, 'xenc:DataReference'));
        $data_ref->setAttribute("URI", '#' . $refuri);
      }
    }
    return;
  }

  /**
   * The function decryptKey.
   */
  public function decryptKey($enc_key) {
    if (!$enc_key->isEncrypted) {
      throw new Exception("Key is not Encrypted");
    }
    if (empty($enc_key->key)) {
      throw new Exception("Key is missing data to perform the decryption");
    }
    return $this->decryptNode($enc_key, FALSE);
  }

  /**
   * The function locateEncryptedData.
   */
  public function locateEncryptedData($element) {
    if ($element instanceof DOMDocument) {
      $doc = $element;
    }
    else {
      $doc = $element->ownerDocument;
    }
    if ($doc) {
      $xpath = new DOMXPath($doc);
      $query = "//*[local-name()='EncryptedData' and namespace-uri()='" . XMLSecEnc::XMLENCNS . "']";
      $nodeset = $xpath->query($query);
      return $nodeset->item(0);
    }
    return NULL;
  }

  /**
   * The function locateKey.
   */
  public function locateKey($node = NULL) {
    if (empty($node)) {
      $node = $this->rawNode;
    }
    if (!$node instanceof DOMNode) {
      return NULL;
    }
    if ($doc = $node->ownerDocument) {
      $xpath = new DOMXPath($doc);
      $xpath->registerNamespace('xmlsecenc', XMLSecEnc::XMLENCNS);
      $query = ".//xmlsecenc:EncryptionMethod";
      $nodeset = $xpath->query($query, $node);
      if ($encmeth = $nodeset->item(0)) {
        $attr_algorithm = $encmeth->getAttribute("Algorithm");
        try {
          $obj_key = new XMLSecurityKey($attr_algorithm, array(
            'type' => 'private',
          ));
        }
        catch (Exception $e) {
          return NULL;
        }
        return $obj_key;
      }
    }
    return NULL;
  }

  /**
   * The function staticLocateKeyInfo.
   */
  private static function staticLocateKeyInfo($obj_base_key = NULL, $node = NULL) {
    if (empty($node) || (!$node instanceof DOMNode)) {
      return NULL;
    }
    $doc = $node->ownerDocument;
    if (!$doc) {
      return NULL;
    }

    $xpath = new DOMXPath($doc);
    $xpath->registerNamespace('xmlsecenc', XMLSecEnc::XMLENCNS);
    $xpath->registerNamespace('xmlsecdsig', XMLSecurityDSig::XMLDSIGNS);
    $query = "./xmlsecdsig:KeyInfo";
    $nodeset = $xpath->query($query, $node);
    $encmeth = $nodeset->item(0);
    if (!$encmeth) {
      /* No KeyInfo in EncryptedData/EncryptedKey. */
      return $obj_base_key;
    }

    foreach ($encmeth->childNodes as $child) {
      switch ($child->localName) {
        case 'KeyName':
          if (!empty($obj_base_key)) {
            $obj_base_key->name = $child->nodeValue;
          }
          break;

        case 'KeyValue':
          foreach ($child->childNodes as $keyval) {
            switch ($keyval->localName) {
              case 'DSAKeyValue':
                throw new Exception("DSAKeyValue currently not supported");

              break;

              case 'RSAKeyValue':
                $modulus = NULL;
                $exponent = NULL;
                if ($modulus_node = $keyval->getElementsByTagName('Modulus')->item(0)) {
                  $modulus = base64_decode($modulus_node->nodeValue);
                }
                if ($exponent_node = $keyval->getElementsByTagName('Exponent')->item(0)) {
                  $exponent = base64_decode($exponent_node->nodeValue);
                }
                if (empty($modulus) || empty($exponent)) {
                  throw new Exception("Missing Modulus or Exponent");
                }
                $public_key = XMLSecurityKey::convertRsa($modulus, $exponent);
                $obj_base_key->loadKey($public_key);
                break;

            }
          }
          break;

        case 'RetrievalMethod':
          $type = $child->getAttribute('Type');
          if ($type !== 'http://www.w3.org/2001/04/xmlenc#EncryptedKey') {
            /* Unsupported key type. */
            break;

          }
          $uri = $child->getAttribute('URI');
          if ($uri[0] !== '#') {
            /* URI not a reference - unsupported. */
            break;

          }
          $id = substr($uri, 1);

          $query = "//xmlsecenc:EncryptedKey[@Id='$id']";
          $key_element = $xpath->query($query)->item(0);
          if (!$key_element) {
            throw new Exception("Unable to locate EncryptedKey with @Id='$id'.");
          }

          return XMLSecurityKey::fromEncryptedKeyElement($key_element);

        case 'EncryptedKey':
          return XMLSecurityKey::fromEncryptedKeyElement($child);

        case 'X509Data':
          if ($x509cert_nodes = $child->getElementsByTagName('X509Certificate')) {
            if ($x509cert_nodes->length > 0) {
              $x509cert = $x509cert_nodes->item(0)->textContent;
              $x509cert = str_replace(array("\r", "\n"), "", $x509cert);
              $x509cert = "-----BEGIN CERTIFICATE-----\n" . chunk_split($x509cert, 64, "\n") . "-----END CERTIFICATE-----\n";
              $obj_base_key->loadKey($x509cert, FALSE, TRUE);
            }
          }
          break;

      }
    }
    return $obj_base_key;
  }

  /**
   * The function locateKeyInfo.
   */
  public function locateKeyInfo($obj_base_key = NULL, $node = NULL) {
    if (empty($node)) {
      $node = $this->rawNode;
    }
    return XMLSecEnc::staticLocateKeyInfo($obj_base_key, $node);
  }

}
